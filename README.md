## XJSFUI

XJSFUI is frontend developed in JSF for IDX, the microservices based indentity management solution. 

IDX has got multiple microservices developed in Spring boot. These microservices provides Rest API which XJSFUI uses for frontend operations. 

Microservices in IDX as as follows:

## XIdentity

XIdentity Microservice provides Identity related services which include:

User Management

Group Management

Organization Management

## XAggregator

XAggregator Microservices provide Aggregation and Provisioning services for IDX

Aggregation Engine: Aggregation Engine provides capability for User, Role, Account and Entitlement aggregation in IDX.

Provisioning Engine: Provisioning Engine provides capability to account/role/entilemanagement for Users in IDX. 

## XCertification

XCertification is Certification Engine of IDX which manages certification of user accounts and entitlements of Users.

## XRequest

XRequest is Request Engine of IDX. Following are high level features:

Request Engine: Request Engine is responsible for managing request lifecycle.

Workflow Engine: Workflow Wngine is responsible for defining approval workflows for different set of users in IDX.


## XScheduler

XScheduler Microservices provides Scheduling capabilities for IDX to run timebound and adhoc Scheduled Jobs.

Job scheduling can be done on periodic basis, adhoc execution or using cron expressions.

XScheduler uses Quartz to perform schduling for the jobs.