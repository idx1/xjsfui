package org.cps.sync.configurator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import org.identityconnectors.common.security.GuardedString;
import org.identityconnectors.framework.api.APIConfiguration;
import org.identityconnectors.framework.api.ConnectorFacade;
import org.identityconnectors.framework.api.ConnectorFacadeFactory;
import org.identityconnectors.framework.api.ConnectorKey;
import org.identityconnectors.framework.common.exceptions.ConnectorException;
import org.identityconnectors.framework.common.objects.AttributeBuilder;
import org.identityconnectors.framework.common.objects.ConnectorMessages;
import org.identityconnectors.framework.common.objects.ConnectorObject;
import org.identityconnectors.framework.common.objects.Name;
import org.identityconnectors.framework.common.objects.ObjectClass;
import org.identityconnectors.framework.common.objects.OperationOptions;
import org.identityconnectors.framework.common.objects.OperationOptionsBuilder;
import org.identityconnectors.framework.common.objects.ResultsHandler;
import org.identityconnectors.framework.common.objects.SyncDelta;
import org.identityconnectors.framework.common.objects.SyncResultsHandler;
import org.identityconnectors.framework.common.objects.SyncToken;
import org.identityconnectors.framework.common.objects.Uid;
import org.identityconnectors.framework.impl.api.APIConfigurationImpl;
import org.identityconnectors.framework.impl.api.local.JavaClassProperties;
import org.identityconnectors.framework.impl.api.local.LocalConnectorInfoImpl;
import org.identityconnectors.framework.impl.api.local.LocalConnectorInfoManagerImpl;

import net.tirasa.connid.bundles.ldap.LdapConfiguration;
import net.tirasa.connid.bundles.ldap.LdapConnection;
import net.tirasa.connid.bundles.ldap.LdapConnector;
import org.identityconnectors.framework.common.objects.Attribute;

public class LDAPConnectorTest implements ResultsHandler, SyncResultsHandler{

	public LDAPConnectorTest() {
	}
	
    public static String HOST="localhost";

    public static Integer PORT=1389;
    
    public static final String ADMIN_DN = "cn=Directory Manager";

    public static final GuardedString ADMIN_PASSWORD = new GuardedString("Welcome1".toCharArray());

    public static final String ACME_DN = "dc=idx,dc=org";

    public static final String BIG_COMPANY_DN = "dc=idx,dc=org";

	private static final String BUGS_BUNNY_DN = "dc=idx,dc=org";

	private static final String SMALL_COMPANY_DN = "o=Anjani Consultants,dc=idx,dc=org";

	private static final String BANGLORE_COMPANY_DN = "ou=Banglore,o=Anjani Consultants,dc=idx,dc=org";
	private static final String DELHI_COMPANY_DN = "ou=Delhi,o=Anjani Consultants,dc=idx,dc=org";
	private static final String SFO_COMPANY_DN = "ou=San Francisco,o=Anjani Consultants,dc=idx,dc=org";
	
    public static LdapConfiguration newConfiguration(final boolean readSchema) {
        LdapConfiguration config = new LdapConfiguration();
        config.setHost(HOST);
        config.setPort(PORT);
        config.setBaseContexts(ACME_DN);
        config.setPrincipal(ADMIN_DN);
        config.setCredentials(ADMIN_PASSWORD);
        config.setReadSchema(readSchema);
        return config;
    }
    
	public static void main(String[] args) throws Exception {
		//provTest();
		reconTest();
	}
	
	public static void reconTest() throws Exception {
		
		LDAPConnectorTest handler = new LDAPConnectorTest();

        LdapConfiguration conf = newConfiguration(false);

        conf.validate();
        
        final ConnectorFacadeFactory factory = ConnectorFacadeFactory.getInstance();
        final APIConfiguration impl = createTestConfiguration(LdapConnector.class, conf);

        impl.getResultsHandlerConfiguration().setFilteredResultsHandlerInValidationMode(true);

        ConnectorFacade connector = factory.newInstance(impl);

        connector.test();

        final OperationOptionsBuilder oob = new OperationOptionsBuilder();
        oob.setAttributesToGet(Arrays.asList(new String[] {
            "displayName", "cn", "sn", "givenName", "employeeNumber", "employeeType", "o", "ou", "mail" }));

        SyncToken token = connector.getLatestSyncToken(ObjectClass.ACCOUNT);

        System.out.println("token : "+ token);

        connector.sync(ObjectClass.ACCOUNT, token, handler, oob.build());

		handler.clear();

	}
	
	
    protected static LdapConfiguration getSimpleConf(final Properties prop) {
    	
		LdapConfiguration configuration = new LdapConfiguration();

		return configuration;

    }

	public static void provTest() throws Exception {
		
		
        LdapConfiguration config = newConfiguration(false);
        config.validate();

		LdapConnection conn = new LdapConnection(config );
        Attributes attrs = conn.getInitialContext().getAttributes(BUGS_BUNNY_DN);

        System.out.println("attrs : "+ attrs);
        
        
        ConnectorFacade facade = newFacade(config);
        //doCreateOrg(facade);
        doCreateAccount(facade, null);
        
        //doCreateGroup(facade);

	}
	
	
	@Override
	public boolean handle(SyncDelta delta) {

		System.out.println("handle delta : " + delta);
		
		return false;
	}

	@Override
	public boolean handle(ConnectorObject connectorObject) {

		System.out.println("handle connectorObject : " + connectorObject);

		return false;
	}	


    private static void doCreateGroup(ConnectorFacade facade) {
        Set<Attribute> attributes = new HashSet<Attribute>();
        Name name = new Name("cn=HR," + SMALL_COMPANY_DN);
        attributes.add(name);
        attributes.add(AttributeBuilder.build("cn", "HR"));
        Uid uid = facade.create(ObjectClass.GROUP, attributes, null);

        ConnectorObject newGroup = facade.getObject(ObjectClass.GROUP, uid, null);
         
        System.out.println("newAccount.getName : "+ newGroup.getName());

        attributes = new HashSet<Attribute>();
        name = new Name("cn=IT Recruitment," + SMALL_COMPANY_DN);
        attributes.add(name);
        attributes.add(AttributeBuilder.build("cn", "IT Recruitment"));
        uid = facade.create(ObjectClass.GROUP, attributes, null);

        newGroup = facade.getObject(ObjectClass.GROUP, uid, null);
         
        System.out.println("newAccount.getName : "+ newGroup.getName());

        attributes = new HashSet<Attribute>();
        name = new Name("cn=Bank Recruitment," + SMALL_COMPANY_DN);
        attributes.add(name);
        attributes.add(AttributeBuilder.build("cn", "Bank Recruitment"));
        uid = facade.create(ObjectClass.GROUP, attributes, null);

        newGroup = facade.getObject(ObjectClass.GROUP, uid, null);
         
        System.out.println("newAccount.getName : "+ newGroup.getName());

        attributes = new HashSet<Attribute>();
        name = new Name("cn=Telecom Recruitment," + SMALL_COMPANY_DN);
        attributes.add(name);
        attributes.add(AttributeBuilder.build("cn", "Telecom Recruitment"));
        uid = facade.create(ObjectClass.GROUP, attributes, null);

        newGroup = facade.getObject(ObjectClass.GROUP, uid, null);
         
        System.out.println("newAccount.getName : "+ newGroup.getName());

    }

    private static void doCreateAccount(ConnectorFacade facade, final OperationOptions options) {
        
        String csvFile = "E:\\work\\flatfile\\sfo.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            int i=0;
            while ((line = br.readLine()) != null) {

            	if(i!=0)
            	{            	
            	
            		
            	
                // use comma as separator
                String[] attrs = line.split(cvsSplitBy);
                
                String uidAtt = attrs[0];
                String cn=attrs[1];
                String givenName = attrs[2];
                String sn=attrs[3];
                String displayName=attrs[4];
                String o=attrs[5];
                String ou=attrs[6];
                String mail=attrs[7];
                String employeeType=attrs[8];
                String employeeNumber=attrs[9];

                System.out.println("displayName : " + displayName);
                
                Set<Attribute> attributes = new HashSet<Attribute>();

                
                Name name = new Name("uid="+uidAtt+"," + SFO_COMPANY_DN);
                attributes.add(name);
                attributes.add(AttributeBuilder.build("uid", uidAtt));
                attributes.add(AttributeBuilder.build("cn", cn));
                attributes.add(AttributeBuilder.build("givenName", givenName));
                attributes.add(AttributeBuilder.build("sn", sn));
                attributes.add(AttributeBuilder.build("mail", mail));
                attributes.add(AttributeBuilder.build("displayName", displayName));
                attributes.add(AttributeBuilder.build("o", o));
                attributes.add(AttributeBuilder.build("ou", ou));
                attributes.add(AttributeBuilder.build("employeeType", employeeType));
                attributes.add(AttributeBuilder.build("employeeNumber", employeeNumber));

                Uid uid = facade.create(ObjectClass.ACCOUNT, attributes, options);

                ConnectorObject newAccount = facade.getObject(ObjectClass.ACCOUNT, uid, options);
                
                System.out.println("newAccount.getName : "+ newAccount.getName());
                
            	}
            	i++;

            }

        } catch (Exception e) {
            e.printStackTrace();
        } 

        
       
        
    }

    private static void doCreateOrg(ConnectorFacade facade) {
        // Let the arbitrary object class be organization.
        Set<Attribute> attributes = new HashSet<Attribute>();
        Name name = new Name("o=Anjani Consultants," + SMALL_COMPANY_DN);
        attributes.add(name);
        attributes.add(AttributeBuilder.build("o", "hq"));
        ObjectClass oclass = new ObjectClass("organization");
        Uid uid = facade.create(oclass, attributes, null);

        ConnectorObject newObject = facade.getObject(oclass, uid, null);

        System.out.println("newObject.getName : "+ newObject.getName());

    }

	    public static ConnectorFacade newFacade(final LdapConfiguration cfg) {
	        ConnectorFacadeFactory factory = ConnectorFacadeFactory.getInstance();
	        APIConfiguration impl = createTestConfiguration(LdapConnector.class, cfg);
	        impl.getResultsHandlerConfiguration().setFilteredResultsHandlerInValidationMode(true);
	        return factory.newInstance(impl);
	    }

		private static APIConfiguration createTestConfiguration(Class<LdapConnector> clazz, LdapConfiguration cfg) {
			   LocalConnectorInfoImpl info = new LocalConnectorInfoImpl();
			    info.setConnectorConfigurationClass(cfg.getClass());
			    info.setConnectorClass(clazz);
			    info.setConnectorDisplayNameKey("DUMMY_DISPLAY_NAME");
			    info.setConnectorKey(new ConnectorKey(clazz.getName() + ".bundle", "1.0", clazz.getName()));
			    info.setMessages(createDummyMessages());
			    try
			    {
			      APIConfigurationImpl rv = LocalConnectorInfoManagerImpl.createDefaultAPIConfiguration(info);
			      rv.setConfigurationProperties(JavaClassProperties.createConfigurationProperties(cfg));
			      
			      info.setDefaultAPIConfiguration(rv);
			      return rv;
			    }
			    catch (Exception e)
			    {
			      throw ConnectorException.wrap(e);
			    }		}

		public static ConnectorMessages createDummyMessages()
		  {
		    return new DummyConnectorMessages(null);
		  }
		  
		  private static class DummyConnectorMessages
		    implements ConnectorMessages
		  {
		    public DummyConnectorMessages(Object object) {
				// TODO Auto-generated constructor stub
			}

			public String format(String key, String dflt, Object... args)
		    {
		      StringBuilder builder = new StringBuilder();
		      builder.append(key);
		      builder.append(": ");
		      String sep = "";
		      for (Object arg : args)
		      {
		        builder.append(sep);
		        builder.append(arg);
		        sep = ", ";
		      }
		      
		      System.out.println("STR : "+ builder.toString());
		      
		      return builder.toString();
		    }
		  }

		    private void clear() {
				// TODO Auto-generated method stub
				
			}

}
