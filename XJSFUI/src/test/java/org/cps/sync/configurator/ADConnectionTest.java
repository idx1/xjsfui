package org.cps.sync.configurator;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Properties;

import org.identityconnectors.common.security.GuardedString;
import org.identityconnectors.framework.api.APIConfiguration;
import org.identityconnectors.framework.api.ConnectorFacade;
import org.identityconnectors.framework.api.ConnectorFacadeFactory;
import org.identityconnectors.framework.api.ConnectorKey;
import org.identityconnectors.framework.common.exceptions.ConnectorException;
import org.identityconnectors.framework.common.objects.ConnectorMessages;
import org.identityconnectors.framework.common.objects.ConnectorObject;
import org.identityconnectors.framework.common.objects.ObjectClass;
import org.identityconnectors.framework.common.objects.OperationOptionsBuilder;
import org.identityconnectors.framework.common.objects.ResultsHandler;
import org.identityconnectors.framework.common.objects.SyncDelta;
import org.identityconnectors.framework.common.objects.SyncResultsHandler;
import org.identityconnectors.framework.common.objects.SyncToken;
import org.identityconnectors.framework.impl.api.APIConfigurationImpl;
import org.identityconnectors.framework.impl.api.local.JavaClassProperties;
import org.identityconnectors.framework.impl.api.local.LocalConnectorInfoImpl;
import org.identityconnectors.framework.impl.api.local.LocalConnectorInfoManagerImpl;

import net.tirasa.connid.bundles.ad.ADConfiguration;
import net.tirasa.connid.bundles.ad.ADConnector;

public class ADConnectionTest implements ResultsHandler, SyncResultsHandler{

	public ADConnectionTest() {
	}

    protected static String BASE_CONTEXT;
    protected static ConnectorFacade connector;

    protected static ADConfiguration conf = null;
    
	public static void main(String[] args) {
		
		ADConnectionTest handler = new ADConnectionTest();

		Properties PROP = new Properties();
		
		PROP.setProperty("host", "192.168.15.5");
		PROP.setProperty("port", "636");
		PROP.setProperty("principal", "cn=administrator,cn=Users,DC=simeio,DC=local");
		PROP.setProperty("credentials", "Suchos123!");
		PROP.setProperty("baseContext", "DC=simeio,DC=local");
		PROP.setProperty("baseContextToSynchronize", "CN=oimtrainee,DC=simeio,DC=local");
		PROP.setProperty("memberships", "CN=Users,CN=CCSFDC=simeio,DC=local ; CN=OIMUsers,DC=simeio,DC=local");

        BASE_CONTEXT = PROP.getProperty("baseContext");

        conf = getSimpleConf(PROP);

        conf.validate();

        final ConnectorFacadeFactory factory = ConnectorFacadeFactory.getInstance();
        final APIConfiguration impl = createTestConfiguration(ADConnector.class, conf);

        impl.getResultsHandlerConfiguration().setFilteredResultsHandlerInValidationMode(true);

        connector = factory.newInstance(impl);

        connector.test();
        
//        // Ask just for sAMAccountName
//        final OperationOptionsBuilder oob = new OperationOptionsBuilder();
//        oob.setAttributesToGet(Arrays.asList(new String[] {
//            "sAMAccountName", "givenName", "memberOf", ADConfiguration.UCCP_FLAG }));
//
//        SyncToken previous = connector.sync(ObjectClass.ACCOUNT, null, handler, oob.build());
//
//		System.out.println("my previous : " + previous);

        
        final OperationOptionsBuilder oob = new OperationOptionsBuilder();
        oob.setAttributesToGet(Arrays.asList(new String[] {
            "sAMAccountName", "givenName", "memberOf", ADConfiguration.UCCP_FLAG }));

        SyncToken token = connector.getLatestSyncToken(ObjectClass.ACCOUNT);
        connector.sync(ObjectClass.ACCOUNT, token, handler, oob.build());

		handler.clear();


	}
	

	private static APIConfiguration createTestConfiguration(Class<ADConnector> clazz, ADConfiguration config) {
        final LocalConnectorInfoImpl info = new LocalConnectorInfoImpl();
        info.setConnectorConfigurationClass(config.getClass());
        info.setConnectorClass(clazz);
        info.setConnectorDisplayNameKey("DUMMY_DISPLAY_NAME");
        info.setConnectorKey(new ConnectorKey(clazz.getName() + ".bundle", "1.0", clazz.getName()));
        info.setMessages(createDummyMessages());
        try {
            final APIConfigurationImpl rv = LocalConnectorInfoManagerImpl.createDefaultAPIConfiguration(info);
            rv.setConfigurationProperties(JavaClassProperties.createConfigurationProperties(config));

            info.setDefaultAPIConfiguration(rv);
            return rv;
        } catch (Exception e) {
            throw ConnectorException.wrap(e);
        }
		
	}

    public static ConnectorMessages createDummyMessages() {
        return new DummyConnectorMessages();
    }

    private static class DummyConnectorMessages implements ConnectorMessages {

        public String format(final String key, final String dflt, final Object... args) {
            final StringBuilder builder = new StringBuilder();
            builder.append(key);
            builder.append(": ");
            String sep = "";
            for (Object arg : args) {
                builder.append(sep);
                builder.append(arg);
                sep = ", ";
            }
            return builder.toString();
        }
    }

    protected static ADConfiguration getSimpleConf(final Properties prop) {

        final ADConfiguration configuration = new ADConfiguration();

        configuration.setUidAttribute("sAMAccountName");
        configuration.setGidAttribute("sAMAccountName");

        configuration.setDefaultPeopleContainer("CN=Users," + BASE_CONTEXT);
        configuration.setDefaultGroupContainer("CN=Users," + BASE_CONTEXT);

        configuration.setObjectClassesToSynchronize("user");

        configuration.setHost(prop.getProperty("host"));
        configuration.setPort(Integer.parseInt(prop.getProperty("port")));

        configuration.setAccountObjectClasses("top", "person", "organizationalPerson", "user");

        configuration.setBaseContextsToSynchronize(prop.getProperty("baseContextToSynchronize"));

        configuration.setUserBaseContexts(BASE_CONTEXT);

        // set default group container as Fgroup search context
        configuration.setGroupBaseContexts(configuration.getDefaultGroupContainer());

        configuration.setPrincipal(prop.getProperty("principal"));

        configuration.setCredentials(new GuardedString(prop.getProperty("credentials").toCharArray()));

        configuration.setMemberships(prop.getProperty("memberships").split(";"));

        configuration.setRetrieveDeletedUser(false);

        configuration.setTrustAllCerts(true);

        configuration.setMembershipsInOr(true);

        configuration.setUserSearchScope("subtree");
        configuration.setGroupSearchScope("subtree");

        configuration.setGroupSearchFilter(
                "(&(cn=GroupTest*)"
                + "(| (memberOf=CN=GroupTestInFilter,CN=Users," + BASE_CONTEXT + ")(cn=GroupTestInFilter)))");



        return configuration;
    }

	@Override
	public boolean handle(ConnectorObject obj) {
		System.out.println("my connector object : " + obj);
		System.out.println("my connector object : " + obj.getUid());
		System.out.println("my connector object : " + obj.getAttributes());

 		return true;
	}

	@Override
	public boolean handle(SyncDelta delta) {
		System.out.println("my connector object : " + delta);


		return false;
	}

    private void clear() {
		// TODO Auto-generated method stub
		
	}


}
