package org.cps.sync.configurator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.cps.identity.common.UserManagerConstants;
import org.cps.sync.model.MATCHING_ACTION;
import org.cps.sync.model.MATCHING_CONDITION;
import org.cps.sync.model.MatchingRule;
import org.cps.sync.model.ProfileAttribute;
import org.cps.sync.model.SYNC_TYPE;
import org.cps.sync.model.SyncProfile;
import org.cps.sync.model.ProfileAttribute.PR_ATTR_TYPE;
import org.idx.application.dob.AGGREGATION_TYPE;
import org.idx.application.dob.AggregationProfileDOB;
import org.idx.application.dob.AppTypeParamsDOB;
import org.idx.application.dob.AppTypeParamsDOB.APP_TYPE_PARAM_TYPE;
import org.idx.application.dob.ApplicationDOB;
import org.idx.application.dob.ApplicationTypeDOB;
import org.idx.application.dob.ConfigurationParamsDOB;
import org.idx.application.dob.ConfigurationParamsDOB.CONFIG_PARAM_TYPE;
import org.idx.application.dob.ConfiguratorDOB;
import org.idx.application.dob.EntitlementDOB;
import org.idx.application.dob.MatchingRuleDOB;
import org.idx.application.dob.ProfileAttributeDOB;
import org.idx.common.rest.ServerResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@ContextConfiguration({"classpath:application-context-test.xml"})
@TestPropertySource(locations="file:E:\\work\\eclipse\\eclipse\\workspace\\HelloBoot\\src\\test\\resources\\application.properties")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RestAPITest {
	
	
	private static final String URL_CREATE_APPLICATION = "http://localhost:8100/aggregator/createApplication";

	private static final String AGGREGATOR_REST_URL = "http://localhost:8100/aggregator";
	private static final String AGGR_GET_APP_TYPE = "/getAppTypeList";
	private static final String AGGR_POST_CREATE_APP_TYPE = "/createApplicationType";

	private static final String APPLICATION_REST_URL = "http://localhost:8100/application";
	private static final String CREATE_ENT_REST_URL = "/createEntitlement";
	private static final String APP_GET_APP_BY_NAME = "/findApplicationByName";
	
	 

	@Test
	@Transactional
	@Rollback(false)
	public void testCreateApplication() {
		testPost();
	}
	
	public static void main (String args[]) {
		testPost();
	      
	}
	
	
	public static void testPost () {
        String csvFile = "C:\\Users\\Admin\\Desktop\\ent.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
        	
            ApplicationDOB app = findApplication("Flat File 1");


            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] country = line.split(cvsSplitBy);
                
                String ENT_CODE = country[6];
                String ENT_VALUE = country[7];
                String ENT_DISPLAY_NAME = country[8];
                String ENT_DESCRIPTION = country[9];
                String ENT_VALID = country[10];
 
                
        		EntitlementDOB ent1 = new EntitlementDOB();
        		
        		ent1.setName(ENT_CODE);
        		ent1.setDisplayName(ENT_DISPLAY_NAME);
        		ent1.setValue(ENT_VALUE);
        		if(ENT_VALID.equals("1"))
            		ent1.setValid(true);
        		else
            		ent1.setValid(false);
        		ent1.setCreatedDate(new Date());
        		ent1.setUpdatedDate(new Date());
        		ent1.setCreatedBy("IDXInternal");
        		ent1.setUpdatedBy("IDXInternal");
        		
        		ent1.setApplication(app);
        		
                System.out.println("ent1 = " + ent1);

        		
        		createEntitlement(ent1);

                

                System.out.println(app);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
	}
		private static ApplicationDOB findApplication(String name) {
			HttpHeaders headers = new HttpHeaders();
		      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		      headers.setContentType(MediaType.APPLICATION_JSON);
 		 
		      RestTemplate restTemplate = new RestTemplate();
		 
		      // Data attached to the request.
		      HttpEntity<ApplicationDOB> requestBody = new HttpEntity<ApplicationDOB>(headers);
		 

		      URI reqURI = null;
		      ResponseEntity<ApplicationDOB>  result = null;
		      ApplicationDOB app1 = null;
		      
			try {
				reqURI = new URI(APPLICATION_REST_URL + APP_GET_APP_BY_NAME);
				
				UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(APPLICATION_REST_URL + APP_GET_APP_BY_NAME)
				        .queryParam("name", name);
				
				String url = URLEncoder.encode(builder.toUriString(), "UTF-8");
				
				System.out.println(url);
				System.out.println(builder.toUriString());
				
//			    result = restTemplate.postForEntity(reqURI, requestBody, ApplicationDOB.class);
			    
			    
			     ResponseEntity<ApplicationDOB> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, requestBody, ApplicationDOB.class);

			    app1 = response.getBody();
//			    
// 			    	System.out.println(app1.getDisplayName());
			     
		    
 			     
 			     
			      System.out.println("Status code:" + response.getStatusCodeValue());
 			      System.out.println("Body:" + app1);
 			      System.out.println("Body:" + response.getBody());
			      System.out.println("class:" + response.getClass());

			} catch (Exception e1) {
			      System.out.println("Body:" + e1.getMessage());
			      System.out.println("Body:" + e1.getCause());

				e1.printStackTrace();
			}



		return app1;
	}

		public static void createEntitlement (EntitlementDOB ent1) {
		
		
		
		
		
		
		HttpHeaders headers = new HttpHeaders();
	      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	      headers.setContentType(MediaType.APPLICATION_JSON);
	 
	      RestTemplate restTemplate = new RestTemplate();
	 
	      
		// Data attached to the request.
	      HttpEntity<EntitlementDOB> requestBody = new HttpEntity<EntitlementDOB>(ent1 , headers);
	 
	      URI reqURI = null;
	      ResponseEntity<EntitlementDOB>  result = null;
		try {
			reqURI = new URI(APPLICATION_REST_URL + CREATE_ENT_REST_URL);
		    result = restTemplate.postForEntity(reqURI, requestBody, EntitlementDOB.class);

		} catch (Exception e1) {
		      System.out.println("Body:" + e1.getMessage());
		      System.out.println("Body:" + e1.getCause());

			e1.printStackTrace();
		}

	      System.out.println("Status code:" + result.getStatusCode());
	      System.out.println("Body:" + result.getBody());
	      
	      if (result.getStatusCode() == HttpStatus.OK) {
	    	  EntitlementDOB e = result.getBody();
	    	  	System.out.println("(Client Side) Entitlement Created: "+ e.getId());
	      }


		
	}
	
	public static void testPost1 () {
		
	      ApplicationTypeDOB appType = new ApplicationTypeDOB();
	      
	      appType.setName("ICF Flat File Connector");
	      appType.setDisplayName("ICF Flat File Connector");
	      appType.setDescription("Flat File connector for procrssing files with csv format");
	      
	      List<AppTypeParamsDOB> appTypeParams = new ArrayList<AppTypeParamsDOB>();
	      
	      
	      AppTypeParamsDOB param1 = new AppTypeParamsDOB();
	      param1.setName("fileName");
	      param1.setLabel("File Name");
	      param1.setType(APP_TYPE_PARAM_TYPE.STRING);
	      param1.setEditable(true);
	      
	      appTypeParams.add(param1);

	      AppTypeParamsDOB param2 = new AppTypeParamsDOB();
	      param2.setName("Bundle Directory");
	      param2.setLabel("Bundle Directory");
	      param2.setType(APP_TYPE_PARAM_TYPE.STRING);
	      param2.setValue("E:/work/flatfile");
	      
	      appTypeParams.add(param2);

	      AppTypeParamsDOB param3 = new AppTypeParamsDOB();
	      param3.setName("Bundle Path");
	      param3.setLabel("Bundle Path");
	      param3.setType(APP_TYPE_PARAM_TYPE.STRING);
	      param3.setValue("org.connid.bundles.flatfile-1.2.jar");
	      
	      appTypeParams.add(param3);

	      AppTypeParamsDOB param4 = new AppTypeParamsDOB();
	      param4.setName("ACCOUNTID");
	      param4.setLabel("Account ID Column");
	      param4.setType(APP_TYPE_PARAM_TYPE.STRING);
	      param4.setEditable(true);
	      
	      appTypeParams.add(param4);

	      AppTypeParamsDOB param5 = new AppTypeParamsDOB();
	      param5.setName("Bundle Name");
	      param5.setLabel("Bundle Name");
	      param5.setType(APP_TYPE_PARAM_TYPE.STRING);
	      param5.setValue("org.connid.bundles.flatfile");
	      
	      
	      appTypeParams.add(param5);

	      AppTypeParamsDOB param6 = new AppTypeParamsDOB();
	      param6.setName("Bundle Version");
	      param6.setLabel("Bundle Version");
	      param6.setType(APP_TYPE_PARAM_TYPE.STRING);
	      param6.setValue("1.2");
	      appTypeParams.add(param6);

	      AppTypeParamsDOB param7 = new AppTypeParamsDOB();
	      param7.setName("Connector Name");
	      param7.setLabel("Connector Name");
	      param7.setType(APP_TYPE_PARAM_TYPE.STRING);
	      param7.setValue("org.identityconnectors.flatfile.FlatFileConnector");
	      
	      appTypeParams.add(param7);


	      AppTypeParamsDOB param8 = new AppTypeParamsDOB();
	      param8.setName("File Encoding");
	      param8.setLabel("File Encoding");
	      param8.setType(APP_TYPE_PARAM_TYPE.STRING);
	      param8.setEditable(true);
	      
	      appTypeParams.add(param8);

	      AppTypeParamsDOB param9 = new AppTypeParamsDOB();
	      param9.setName("File Delimiter");
	      param9.setLabel("File Delimiter");
	      param9.setType(APP_TYPE_PARAM_TYPE.STRING);
	      param9.setEditable(true);
	      
	      appTypeParams.add(param9);
	      
		  appType.setAppTypeParams(appTypeParams);

	      createAppType(appType);

	      appType = new ApplicationTypeDOB();
	      
	      appType.setName("Active Directory");
	      appType.setDisplayName("Active Directory");
	      appType.setDescription("Active Directory");

	      createAppType(appType);

	      appType = new ApplicationTypeDOB();
	      
	      appType.setName("Workday");
	      appType.setDisplayName("Workday");
	      appType.setDescription("Workday");

	      createAppType(appType);

	      appType.setName("Office 360");
	      appType.setDisplayName("Office 360");
	      appType.setDescription("Office 360 Conenctor v1.0");

	      createAppType(appType);

	}
	
	public static void createAppType(ApplicationTypeDOB appType) {

		HttpHeaders headers = new HttpHeaders();
	      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	      headers.setContentType(MediaType.APPLICATION_JSON);
	 
	      RestTemplate restTemplate = new RestTemplate();
	 
	      
		// Data attached to the request.
	      HttpEntity<ApplicationTypeDOB> requestBody = new HttpEntity<ApplicationTypeDOB>(appType , headers);
	 
	      URI reqURI = null;
	      ResponseEntity<ApplicationTypeDOB>  result = null;
		try {
			reqURI = new URI(AGGREGATOR_REST_URL + AGGR_POST_CREATE_APP_TYPE);
		    result = restTemplate.postForEntity(reqURI, requestBody, ApplicationTypeDOB.class);

		} catch (Exception e1) {
		      System.out.println("Body:" + e1.getMessage());
		      System.out.println("Body:" + e1.getCause());

		      e1.printStackTrace();
		}

	      System.out.println("Status code:" + result.getStatusCode());
	      System.out.println("Body:" + result.getBody());
	      
	      if (result.getStatusCode() == HttpStatus.OK) {
	    	  	ApplicationTypeDOB e = result.getBody();
	    	  	System.out.println("(Client Side) Application Type Created: "+ e.getId());
	      }


	}
	
	
		public static void testGet () {

			
			ApplicationDOB app = new ApplicationDOB();
		
		app.setName("RestAPITestApp5");
		
		
		ConfiguratorDOB configurator = new ConfiguratorDOB();
		
		configurator.setName("RestAPITestApp5Configurator");
		
		configurator.setDisplayName("RestAPITestApp5Configurator");
		
		configurator.setProfileName("FlatFileTest");
		
		List<ConfigurationParamsDOB> configParams = new ArrayList<ConfigurationParamsDOB>();
		configurator.setConfigParams(configParams );
		
		ConfigurationParamsDOB configParams1 = new ConfigurationParamsDOB();
		
		configParams1.setName("name");
		configParams1.setValue("Test");
		configParams1.setType(CONFIG_PARAM_TYPE.STRING);
		
		configParams.add(configParams1);
		
		configurator.setConfigParams(configParams);
		
		app.setConfigurator(configurator );
		
		AggregationProfileDOB profile = new AggregationProfileDOB();
		
		profile.setName("FlatFileTest");
		profile.setAggregationType(AGGREGATION_TYPE.USER);
		profile.setTableName("SY_FFTest");
		
		MatchingRuleDOB rule = new MatchingRuleDOB();
		
		rule.setIdxAttribute("accountid");
		rule.setTgtAttribute(UserManagerConstants.EMPLOYEE_ID);
		//rule.setMatchingAction(org.idx.application.dob.MATCHING_ACTION.CREATE);
		rule.setMatchingCondition(org.idx.application.dob.MATCHING_CONDITION.EQUALS);
		
		profile.setMatchingRule(rule);
		
		ProfileAttributeDOB attr1 = new ProfileAttributeDOB();
		
		attr1.setName("accountid");
		attr1.setTargetAttrName(UserManagerConstants.EMPLOYEE_ID);  
		attr1.setType(org.idx.application.dob.ProfileAttributeDOB.PR_ATTR_TYPE.STRING);
		attr1.setKey(true);
 		
		ProfileAttributeDOB attr2 = new ProfileAttributeDOB();
		
		attr2.setName("firstname");
		attr2.setTargetAttrName(UserManagerConstants.FIRST_NAME);
		attr2.setType(org.idx.application.dob.ProfileAttributeDOB.PR_ATTR_TYPE.STRING);
		
		ProfileAttributeDOB attr3 = new ProfileAttributeDOB();
		attr3.setName("lastname");
		attr3.setTargetAttrName(UserManagerConstants.LAST_NAME);
		attr3.setType(org.idx.application.dob.ProfileAttributeDOB.PR_ATTR_TYPE.STRING);

		ProfileAttributeDOB attr4 = new ProfileAttributeDOB();
		attr4.setName(UserManagerConstants.EMAIL);
		attr4.setTargetAttrName("email");
		attr4.setType(org.idx.application.dob.ProfileAttributeDOB.PR_ATTR_TYPE.STRING);

		ProfileAttributeDOB attr5 = new ProfileAttributeDOB();
		attr5.setName("changeNumber");
		attr5.setTargetAttrName("changeNumber");
		attr5.setType(org.idx.application.dob.ProfileAttributeDOB.PR_ATTR_TYPE.STRING);
		
		ProfileAttributeDOB attr6 = new ProfileAttributeDOB();
		attr6.setName("__UID__");
		attr6.setTargetAttrName("__UID__");
		attr6.setType(org.idx.application.dob.ProfileAttributeDOB.PR_ATTR_TYPE.STRING);
		
		ProfileAttributeDOB attr7 = new ProfileAttributeDOB();
		attr7.setName("__NAME__");
		attr7.setTargetAttrName("__NAME__");
		attr7.setType(org.idx.application.dob.ProfileAttributeDOB.PR_ATTR_TYPE.STRING);
		
 		List<ProfileAttributeDOB> profileAttrs = new ArrayList<ProfileAttributeDOB>();
		profileAttrs.add(attr1);
		profileAttrs.add(attr2);
		profileAttrs.add(attr3);
		profileAttrs.add(attr4);
		profileAttrs.add(attr5);
		profileAttrs.add(attr6);
		profileAttrs.add(attr7);
		
		profile.setProfileAttr(profileAttrs);

		app.setProfile(profile);
		
		
		HttpHeaders headers = new HttpHeaders();
	      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	      headers.setContentType(MediaType.APPLICATION_JSON);
	 
	      RestTemplate restTemplate = new RestTemplate();
	 
	      // Data attached to the request.
	      HttpEntity<ApplicationDOB[]> requestBody = new HttpEntity<ApplicationDOB[]>(headers);
	 

	      URI reqURI = null;
	      ResponseEntity<ApplicationDOB>  result = null;
		try {
			reqURI = new URI(AGGREGATOR_REST_URL + AGGR_GET_APP_TYPE);
//		    result = restTemplate.postForEntity(reqURI, requestBody, ApplicationDOB.class);
		    
		    
		    ResponseEntity<ApplicationDOB[]> response = restTemplate.exchange(reqURI, HttpMethod.GET, requestBody, ApplicationDOB[].class);

		    ApplicationDOB[] result1 = response.getBody();
		    
		    for(ApplicationDOB app1: result1) {
		    	System.out.println(app1.getDisplayName());
		    }
	    
		      System.out.println("Status code:" + response.getStatusCode());
		      System.out.println("Body:" + result1);

		} catch (Exception e1) {
		      System.out.println("Body:" + e1.getMessage());
		      System.out.println("Body:" + e1.getCause());

			e1.printStackTrace();
		}



		 

		
	      // Send request with POST method.
	      
//	      URI reqURI = null;
//	      ResponseEntity<ApplicationDOB>  result = null;
//		try {
//			reqURI = new URI(URL_CREATE_APPLICATION);
//		    result = restTemplate.postForEntity(reqURI, requestBody, ApplicationDOB.class);
//
//		} catch (Exception e1) {
//		      System.out.println("Body:" + e1.getMessage());
//		      System.out.println("Body:" + e1.getCause());
//
//			e1.printStackTrace();
//		}
//
//	      System.out.println("Status code:" + result.getStatusCode());
//	      System.out.println("Body:" + result.getBody());
//	      
//	      if (result.getStatusCode() == HttpStatus.OK) {
//	    	  	ApplicationDOB e = result.getBody();
//	    	  	System.out.println("(Client Side) Application Created: "+ e.getId());
//	      }
//	      
	}
	


}
