package org.cps.identity.utils;

import org.cps.identity.dao.UserManagerDAO;
import org.cps.identity.exception.UserManagerException;
import org.cps.identity.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserLoginGenerator {
	
	@Autowired
	private UserManagerDAO userManagerDAO;


	public String generateLogin(User user) throws UserManagerException {
		//TODO: This is going to be plugin based.

		String userLogin = generateDefaultUserManagerLogin(user);
		
		return userLogin;
	}


	private String generateDefaultUserManagerLogin(User user) throws UserManagerException {
		
		String userLogin = null;
		
		String fName = user.getFirstName();
		String lName = user.getLastName();
		
		//Note: Rule 1 : use first name
		boolean exists = checkIfUserLoginExists(fName);
		
		if(!exists)
			return fName;
		
		//Note: Rule 2: fist letter of first name plus first 5 letters of Last Name
		String str1 = fName.substring(0,1);
		String str2 = null;
		
		if(lName.length() >= 5)
			str2 = lName.substring(0,5);
		else
			str2 = lName;
		
		exists = checkIfUserLoginExists(str1+str2);

		if(!exists)
			return str1+str2;
		
		// Note: Rule 3: fist letter of first name plus first 4 letters of Last Name plus number

		str1 = fName.substring(0,1);
		str2 = null;
		
		if(lName.length() >= 4)
			str2 = lName.substring(0,4);
		else
			str2 = lName;

		int i=1;
		
		userLogin = str1+str2+i;
		
		boolean userLoginExists = true;

		while(checkIfUserLoginExists(userLogin)) {
			System.out.println("userLogin : " + userLogin);

			userLogin = str1+str2+i;
			
			i++;
		}
		

		System.out.println("userLogin : " + userLogin);
		
		
		return userLogin;
	}


	private boolean checkIfUserLoginExists(String userName) throws UserManagerException {
		return userManagerDAO.checkIfUserLoginExists(userName);
	}

}
