package org.cps.identity.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import org.cps.identity.service.UserManagerService;
import org.cps.framework.jms.listener.UserOperationsMessageProducer;
import org.cps.framework.notification.EmailService;
import org.cps.framework.notification.Mail;
import org.cps.identity.common.PasswordUtils;
import org.cps.identity.model.User;

@Service("userManagerUtils")
public class UserManagerUtils {

	@Autowired
	static UserManagerService userService;
	
	@Autowired
	UserOperationsMessageProducer userOperationsMessageProducer;

    @Autowired
    private EmailService emailService;

	@Async
	public User performUserCreatePostProcessing(User user) {
		assignUserToGroup(user);
		
		// Evaluate User Policy
		evaluateUserPolicy(user);
		
		// Do custom Post processing
		performCustomPostProcessingonCreate(user);
		
		sendNotification(user);
		
		return user;

	}
	
 
	private void sendNotification(User user) {
		// TODO Auto-generated method stub
		
        Mail mail = new Mail();
        mail.setFrom("no-reply@idxGovernance.com");
        mail.setTo("writetoanjani54@gmail.com");
        mail.setSubject("Sending Email with Thymeleaf HTML Template Example");

        Map<String, String> model = new HashMap<String, String>();
        model.put("name", user.getFirstName() + " " + user.getLastName());
        model.put("userName", user.getUserName());
        model.put("password", user.getPassword());
        model.put("idxSignature", "IDX Provisioning Team");
        model.put("location", "India");
        model.put("signature", "https://idxGovernance.com");
        mail.setModel(model);

        try {
			emailService.sendSimpleMessage(mail, "create-user");
		} catch (Exception e) {
			e.printStackTrace();
			sendToFailedNotificationQueue();
		} 


		
	}

	//TODO: Failed Notification should be sent to queue to processing later
	private void sendToFailedNotificationQueue() {

		
	}

	private void assignUserToGroup(User user) {
		// Add to CPS Group	
		
		// Get Users Org, based on org policy decide on people group
		
		// Get User Type, based on user type decide on users group
		
		 
	}

	private void evaluateUserPolicy(User user) {
		// Get Users group and provision application
		
	}

	private void performCustomPostProcessingonCreate(User user) {
		// TODO Auto-generated method stub
		
	}



}
