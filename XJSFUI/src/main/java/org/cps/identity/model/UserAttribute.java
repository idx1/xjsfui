package org.cps.identity.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="USR_ATTR")
@Data
public class UserAttribute {

	@Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
      
	@Column(nullable = false)
    private String name;

	@Column(nullable = false)
    private String displayName;

	@Column(nullable = false)
    private USER_ATTR_TYPE userAttrType;
	
	@Column(nullable = false)
    private String category;


}
