package org.cps.identity.model;

public enum USER_ATTR_TYPE {

	STRING (1),
	NUMBER (2),
	BOOLEAN (3),
	SECRET (4),
	DATE (5);
	
	private final int userAttrType;
	
	USER_ATTR_TYPE(int userAttrType){
		this.userAttrType=userAttrType;
	}

	public int getUserAttrType(){
		return userAttrType;
	}

}

