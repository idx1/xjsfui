package org.cps.identity.common;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class IdentityOperationResult {
	public String id;
	public Map	getFailedResults;
	public String	getStatus;
	public List	getSucceededResults;
}
