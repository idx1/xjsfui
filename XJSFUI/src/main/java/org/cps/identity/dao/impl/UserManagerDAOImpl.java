package org.cps.identity.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.cps.framework.common.SearchCriteria;
import org.cps.framework.common.SearchCriteria.SEARCH_TYPE;
import org.cps.framework.common.SearchValue;
import org.cps.identity.dao.UserManagerDAO;
import org.cps.identity.exception.UserManagerException;
import org.cps.identity.model.User;
import org.cps.identity.model.UserAttribute;
import org.cps.web.exception.UserCreationException;
import org.cps.web.ui.beans.UserSearchCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public class UserManagerDAOImpl implements UserManagerDAO {

         
    @PersistenceContext
    private EntityManager manager;

	public List<User> findUser(SearchCriteria criteria) throws UserManagerException{
		
		List<User> list = new ArrayList<User>();
		
		Map<String, SearchValue> searchCriteria = criteria.getSearchCriteria();
		SEARCH_TYPE searchType = criteria.getSearchType();

		return list;
	}

	public User createUser(User user) throws UserManagerException{
		
		User savedUser = null;
		
		try {
		manager.persist(user);    	
//		logger.debug("Person saved successfully, Person Details="+user);
		
		Query query = manager.createQuery("Select u from User u where u.userName= :userName");
		query.setParameter("userName", user.getUserName());
		
		System.out.println("user.getUserName : "+ user.getUserName());
		
		savedUser = (User) query.getSingleResult();
		
		}catch(Exception e) {
			throw new UserManagerException(e.getMessage());
		}
		return savedUser;
	}

	public boolean checkIfUserLoginExists(String userLogin) throws UserManagerException{
		Query query = manager.createQuery("Select u from User u where u.userName= :userName");
		query.setParameter("userName", userLogin);
		
		List result = query.getResultList();
		
		if(result.size() > 0)
			return true;

		return false;
	}

	@Override
	public void login(String userName, String password) throws UserManagerException{
		System.out.println("DAO userName : " + userName);
		System.out.println("DAO password : " + password);
	}

	@Override
	public List<User> searchUser(UserSearchCriteria searchCriteria) throws UserManagerException{

		Query query = manager.createQuery("Select u from User u");
		
		List<User> users = query.getResultList();

		return users;
	}

	@Override
	public List<UserAttribute> getUserAttributes() throws UserManagerException {
		Query query = manager.createQuery("Select u from UserAttribute u");
		
		List<UserAttribute> userAttrs = query.getResultList();
		return userAttrs;
	}

 	 
}
