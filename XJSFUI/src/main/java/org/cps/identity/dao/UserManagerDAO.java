package org.cps.identity.dao;

import java.util.List;

import org.cps.framework.common.SearchCriteria;
import org.cps.identity.exception.UserManagerException;
import org.cps.identity.model.User;
import org.cps.identity.model.UserAttribute;
import org.cps.web.exception.UserCreationException;
import org.cps.web.ui.beans.UserSearchCriteria;

  
public interface UserManagerDAO {

	public List<User> findUser(SearchCriteria criteria) throws UserManagerException;

	public User createUser(User user) throws UserManagerException;

	public boolean checkIfUserLoginExists(String userLogin) throws UserManagerException;

	public void login(String userName, String password) throws UserManagerException;

	public List<User> searchUser(UserSearchCriteria searchCriteria) throws UserManagerException;

	public List<UserAttribute> getUserAttributes() throws UserManagerException;

}
