package org.cps.identity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.cps.framework.common.SearchCriteria;
import org.cps.identity.dao.GroupManagerDAO;
import org.cps.identity.dao.RoleManagerDAO;
import org.cps.identity.dao.UserManagerDAO;
import org.cps.identity.model.Group;
import org.cps.identity.model.Role;
import org.cps.identity.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public class RoleManagerDAOImpl implements RoleManagerDAO {

	@PersistenceContext
    private EntityManager manager;

	public Role createRole(Role role) {
		manager.persist(role);    	
		role = findRoleByName(role.getName());
		return role;
	}

 
	@Override
	public void updateRole(Role role) {

		
	}

	@Override
	public void addUserToRole(Role role, User user) {
	}

	@Override
	public Role findRoleByName(String roleName) {
		Query query = manager.createQuery("Select r from Role u where u.name= :name");
		query.setParameter("name", roleName);
		
		Role role = (Role) query.getSingleResult();

		System.out.println("role name : "+ role.getName());
		
		return null;
	}

	@Override
	public Role findRoleByID(long id) {

		return null;
	}

	@Override
	public List<Role> findRole(SearchCriteria criteria) {
		// TODO Auto-generated method stub
		return null;
	}

	 

}
