package org.cps.identity.dao;

import java.util.List;

import org.cps.framework.common.SearchCriteria;
import org.cps.identity.model.Group;
import org.cps.identity.model.Role;
import org.cps.identity.model.User;

public interface RoleManagerDAO {

	Role createRole(Role role);

	void updateRole(Role role);

	void addUserToRole(Role role, User user);

	Role findRoleByName(String roleName);

	Role findRoleByID(long id);

	List<Role> findRole(SearchCriteria criteria);

}
