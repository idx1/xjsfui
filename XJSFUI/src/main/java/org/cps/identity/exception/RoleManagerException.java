package org.cps.identity.exception;

public class RoleManagerException extends Exception {

	public RoleManagerException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RoleManagerException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public RoleManagerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RoleManagerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RoleManagerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
