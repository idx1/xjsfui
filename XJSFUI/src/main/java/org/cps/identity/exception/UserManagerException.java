package org.cps.identity.exception;

public class UserManagerException extends Exception {

	public UserManagerException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserManagerException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public UserManagerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserManagerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UserManagerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
