package org.cps.identity.exception;

public class OrgManagerException extends Exception {

	public OrgManagerException() {
		// TODO Auto-generated constructor stub
	}

	public OrgManagerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public OrgManagerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public OrgManagerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public OrgManagerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
