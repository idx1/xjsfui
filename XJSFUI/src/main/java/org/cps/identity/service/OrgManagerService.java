package org.cps.identity.service;

import java.util.List;

import org.cps.framework.common.SearchCriteria;
import org.cps.framework.exception.CPSAuthenticationException;
import org.cps.identity.common.IdentityOperationResult;
import org.cps.identity.exception.OrgManagerException;
import org.cps.identity.exception.RoleManagerException;
import org.cps.identity.model.Organization;
import org.cps.identity.model.Role;
import org.cps.identity.model.User;
import org.cps.web.exception.RoleCreationException;
import org.cps.web.exception.UserCreationException;
import org.cps.web.ui.beans.UserSearchCriteria;

public interface OrgManagerService {

	public Organization createOrganization(Organization org) throws OrgManagerException;

	public IdentityOperationResult updateOrganization(Organization org) throws OrgManagerException;

	public IdentityOperationResult deleteOrganization(Organization org) throws OrgManagerException;

	public Organization findOrganization(String orgName) throws OrgManagerException;

	public Organization findOrganization(long id) throws OrgManagerException;	

}
