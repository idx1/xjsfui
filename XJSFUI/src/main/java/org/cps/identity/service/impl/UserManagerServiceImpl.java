package org.cps.identity.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.cps.framework.common.CPSConstants;
import org.cps.framework.common.SearchCriteria;
import org.cps.framework.exception.CPSAuthenticationException;
import org.cps.framework.jms.listener.UserOperationsMessageProducer;
import org.cps.framework.notification.EmailService;
import org.cps.framework.notification.Mail;
import org.cps.identity.model.User;
import org.cps.identity.model.UserAttribute;
import org.cps.identity.service.UserManagerService;
import org.cps.identity.utils.UserLoginGenerator;
import org.cps.identity.utils.UserManagerUtils;
import org.cps.web.exception.UserCreationException;
import org.cps.web.ui.beans.UserSearchCriteria;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.idx.identity.dob.UserAttributeDOB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.cps.identity.common.IdentityOperationResult;
import org.cps.identity.dao.UserManagerDAO;
import org.cps.identity.exception.UserManagerException;
 
@Service("userManagerService")
public class UserManagerServiceImpl implements UserManagerService{

	@Autowired
	private UserManagerDAO userManagerDAO;

	@Autowired
	UserLoginGenerator userLoginGenerator;
		
	@Autowired
	UserManagerUtils userManagerUtils;

	private Mapper mapper = new DozerBeanMapper();


	public List<User> findUser(SearchCriteria criteria) throws UserManagerException {
		List<User> users = userManagerDAO.findUser(criteria);	
		return users;
	}

	public User createUser(User user) throws UserManagerException{		
		String userName = user.getUserName();
		
		System.out.println("USER KA NAAM : " + userName);

		boolean exists = userManagerDAO.checkIfUserLoginExists(userName);
		 
		if(exists)
			throw new UserManagerException(CPSConstants.USER_ALREADY_EXISTS);
		
		if(userName == null) {
			userName = generateUserLogin(user);
			user.setUserName(userName);
		}

		User usr = userManagerDAO.createUser(user);

		usr = userManagerUtils.performUserCreatePostProcessing(usr);
		
		return usr;
	}

	private String generateUserLogin(User user) throws UserManagerException {
		String userName = null;
		
		userName = userLoginGenerator.generateLogin(user);

		return userName;
	}


	public boolean checkIfUserLoginExists(String userLogin) throws UserManagerException {
		return userManagerDAO.checkIfUserLoginExists(userLogin);

	}

	@Override
	public void login(String userName, String password) throws UserManagerException {
		userManagerDAO.login(userName, password);		
	}

	@Override
	public IdentityOperationResult updateUser(User user) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult updateUser(long id, String attrName, Object attrValue) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult updateUser(long id, Map attributes) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult delete(List userIDs, boolean isUserLogin) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult delete(String userID, boolean isUserLogin) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult delete(String attributeName, Object attributeValue) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult disableUser(List userIDs, boolean isUserLogin) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult disableUser(String userID, boolean isUserLogin) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult enableUser(List userIDs, boolean isUserLogin) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult enableUser(String userID, boolean isUserLogin) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult lockUser(List userIDs, boolean isUserLogin) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult lockUser(String userID, boolean isUserLogin) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult unlockUser(List userIDs, boolean isUserLogin) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public IdentityOperationResult unlockUser(String userID, boolean isUserLogin) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public User findUser(List userIDs, boolean isUserLogin) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public List<User> findUser(String attributeName, Object attributeValue) throws UserManagerException {
		throw new UserManagerException("Not Implemented");
	}

	@Override
	public List<UserAttributeDOB> getUserAttributes() throws UserManagerException {
		List<UserAttribute> userAttrs = userManagerDAO.getUserAttributes();
		List<UserAttributeDOB> userAttrDOBs = new ArrayList<UserAttributeDOB>();
		
		for(UserAttribute userAttr:userAttrs) {
			UserAttributeDOB userAttrDOB =  mapper.map(userAttr, UserAttributeDOB.class);
			userAttrDOBs.add(userAttrDOB);
		}
		return userAttrDOBs;
	}
 
}
