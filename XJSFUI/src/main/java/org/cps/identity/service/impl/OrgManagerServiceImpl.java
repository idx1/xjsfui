package org.cps.identity.service.impl;

import java.util.List;


import org.cps.framework.common.SearchCriteria;
import org.cps.identity.common.IdentityOperationResult;
import org.cps.identity.dao.GroupManagerDAO;
import org.cps.identity.dao.OrgManagerDAO;
import org.cps.identity.dao.RoleManagerDAO;
import org.cps.identity.dao.UserManagerDAO;
import org.cps.identity.exception.OrgManagerException;
import org.cps.identity.exception.RoleManagerException;
import org.cps.identity.model.Group;
import org.cps.identity.model.Organization;
import org.cps.identity.model.Role;
import org.cps.identity.model.User;
import org.cps.identity.service.GroupManagerService;
import org.cps.identity.service.OrgManagerService;
import org.cps.identity.service.RoleManagerService;
import org.cps.identity.service.UserManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("orgManagerService")
public class OrgManagerServiceImpl implements OrgManagerService {

		@Autowired
		private OrgManagerDAO orgManagerDAO;

		@Override
		public Organization createOrganization(Organization org) throws OrgManagerException {

			return orgManagerDAO.createOrg(org);
		}

		@Override
		public IdentityOperationResult updateOrganization(Organization org) throws OrgManagerException {

			throw new OrgManagerException("Not Implemented");
		}

		@Override
		public IdentityOperationResult deleteOrganization(Organization org) throws OrgManagerException {
			throw new OrgManagerException("Not Implemented");
		}

		@Override
		public Organization findOrganization(String orgName) throws OrgManagerException {
			throw new OrgManagerException("Not Implemented");
		}

		@Override
		public Organization findOrganization(long id) throws OrgManagerException {
			throw new OrgManagerException("Not Implemented");
		}

		
 }
