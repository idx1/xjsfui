package org.cps.identity.service;

import java.util.List;
import java.util.Map;

import org.cps.framework.common.SearchCriteria;
import org.cps.identity.common.IdentityOperationResult;
import org.cps.identity.exception.UserManagerException;
import org.cps.identity.model.User;
import org.cps.identity.model.UserAttribute;
import org.cps.web.ui.beans.UserSearchCriteria;
import org.idx.identity.dob.UserAttributeDOB;
 
 
public interface UserManagerService {
	
	public User createUser(User user) throws UserManagerException;
	
	public IdentityOperationResult updateUser(User user) throws UserManagerException;

	public IdentityOperationResult updateUser(long id, String attrName, Object attrValue) throws UserManagerException;
	
	public IdentityOperationResult updateUser(long id, Map attributes) throws UserManagerException;
	
	public IdentityOperationResult delete(List userIDs, boolean isUserLogin) throws UserManagerException;

	public IdentityOperationResult delete(String userID, boolean isUserLogin) throws UserManagerException;
	
	public IdentityOperationResult delete(String attributeName, Object attributeValue) throws UserManagerException;
	
	public IdentityOperationResult disableUser(List userIDs, boolean isUserLogin) throws UserManagerException;

	public IdentityOperationResult disableUser(String userID, boolean isUserLogin) throws UserManagerException;

	public IdentityOperationResult enableUser(List userIDs, boolean isUserLogin) throws UserManagerException;

	public IdentityOperationResult enableUser(String userID, boolean isUserLogin) throws UserManagerException;

	public IdentityOperationResult lockUser(List userIDs, boolean isUserLogin) throws UserManagerException;

	public IdentityOperationResult lockUser(String userID, boolean isUserLogin) throws UserManagerException;

	public IdentityOperationResult unlockUser(List userIDs, boolean isUserLogin) throws UserManagerException;

	public IdentityOperationResult unlockUser(String userID, boolean isUserLogin) throws UserManagerException;

	public boolean checkIfUserLoginExists(String userLogin) throws UserManagerException;
	
	public void login(String userName, String password) throws UserManagerException;

	public List<User> findUser(SearchCriteria criteria) throws UserManagerException;

	public User findUser(List userIDs, boolean isUserLogin) throws UserManagerException;

	public List<User> findUser(String attributeName, Object attributeValue) throws UserManagerException;

	public List<UserAttributeDOB> getUserAttributes() throws UserManagerException;

}

