package org.cps.identity.service.impl;

import java.util.List;


import org.cps.framework.common.SearchCriteria;
import org.cps.identity.common.IdentityOperationResult;
import org.cps.identity.dao.GroupManagerDAO;
import org.cps.identity.dao.RoleManagerDAO;
import org.cps.identity.dao.UserManagerDAO;
import org.cps.identity.exception.RoleManagerException;
import org.cps.identity.model.Group;
import org.cps.identity.model.Role;
import org.cps.identity.model.User;
import org.cps.identity.service.GroupManagerService;
import org.cps.identity.service.RoleManagerService;
import org.cps.identity.service.UserManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("roleManagerService")
public class RoleManagerServiceImpl implements RoleManagerService {

	

		@Autowired
		private RoleManagerDAO roleManagerDAO;

		
	public Role createRole(Role role) {
		return roleManagerDAO.createRole(role);
	}

	public IdentityOperationResult updateRole(Role role) {
		roleManagerDAO.updateRole(role);
		return null;
	}

	public IdentityOperationResult addUserToRole(Role role, User user) {
		roleManagerDAO.addUserToRole(role, user);
		return null;
	}

	public Role findRoleByName(String roleName) {
		return roleManagerDAO.findRoleByName(roleName);
	}

	@Override
	public Role findRole(long id) throws RoleManagerException {
		return roleManagerDAO.findRoleByID(id);
	}

	@Override
	public List<Role> findRole(SearchCriteria criteria) throws RoleManagerException {
		return roleManagerDAO.findRole(criteria);
	}

	@Override
	public IdentityOperationResult deleteRole(Role role) throws RoleManagerException {

		throw new RoleManagerException("Not Implemented");		
	}

	@Override
	public Role findRole(String roleName) throws RoleManagerException {
		throw new RoleManagerException("Not Implemented");		
	}

	@Override
	public IdentityOperationResult addUserToRole(long roleId, long userId) throws RoleManagerException {
		throw new RoleManagerException("Not Implemented");		
	}

	@Override
	public IdentityOperationResult addUsersToRole(long roleId, List userIds) throws RoleManagerException {
		throw new RoleManagerException("Not Implemented");		
	}

	@Override
	public IdentityOperationResult removeUserToRole(long roleId, long userId) throws RoleManagerException {
		throw new RoleManagerException("Not Implemented");		
	}

	@Override
	public IdentityOperationResult removeUsersToRole(long roleId, List userIds) throws RoleManagerException {
		throw new RoleManagerException("Not Implemented");		
	}

	@Override
	public List<User> getRoleMembers(long roleId, boolean directAndIndirect) throws RoleManagerException{
		throw new RoleManagerException("Not Implemented");		
	}

 }
