package org.cps.identity.service;

import java.util.List;

import org.cps.framework.common.SearchCriteria;
import org.cps.framework.exception.CPSAuthenticationException;
import org.cps.identity.common.IdentityOperationResult;
import org.cps.identity.exception.RoleManagerException;
import org.cps.identity.model.Role;
import org.cps.identity.model.User;
import org.cps.web.exception.RoleCreationException;
import org.cps.web.exception.UserCreationException;
import org.cps.web.ui.beans.UserSearchCriteria;

public interface RoleManagerService {
	
 	
	public Role createRole(Role role) throws RoleManagerException;
	
	public IdentityOperationResult updateRole(Role role) throws RoleManagerException;

	public IdentityOperationResult deleteRole(Role role) throws RoleManagerException;

	public IdentityOperationResult addUserToRole(Role role, User user) throws RoleManagerException;
	 
	public Role findRole(String roleName) throws RoleManagerException;
	
	public Role findRole(long id) throws RoleManagerException;

	public List<Role> findRole(SearchCriteria criteria) throws RoleManagerException;
	
	public IdentityOperationResult addUserToRole(long roleId, long userId) throws RoleManagerException;

	public IdentityOperationResult addUsersToRole(long roleId, List userIds) throws RoleManagerException;

	public IdentityOperationResult removeUserToRole(long roleId, long userId) throws RoleManagerException;

	public IdentityOperationResult removeUsersToRole(long roleId, List userIds) throws RoleManagerException;

	public List<User> getRoleMembers(long roleId, boolean directAndIndirect) throws RoleManagerException;
	
	

}
