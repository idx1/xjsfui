package org.cps.prov.dao;

 
import org.cps.prov.model.Application;
import org.cps.prov.model.ApplicationDefinition;

public interface ApplicationDAO {
	
	public void createAppDefinition(ApplicationDefinition appDef);
	
	public void createApplication(Application app);
	
	public ApplicationDefinition findAppDefinitionByName(String appDefName);
	
	public ApplicationDefinition findAppDefinitionByID(long appDefID);

	public Application findApplicationByName(String appDefName);
	
	public Application findApplicationByID(long appDefID);

}
