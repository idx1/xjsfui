package org.cps.prov.dao;

 
import javax.persistence.Query;

import org.cps.prov.model.Application;
import org.cps.prov.model.ApplicationDefinition;
import org.cps.prov.model.ProvisioningPolicy;

public interface ProvEngineDAO {
	
	public void createProvisioningPolicy(ProvisioningPolicy provPolicy);

	public ProvisioningPolicy findProvPolicyByName(String policyName);

	public ProvisioningPolicy findProvPolicyByID(long id);

}
