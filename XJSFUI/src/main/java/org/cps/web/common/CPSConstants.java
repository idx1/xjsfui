package org.cps.web.common;

public class CPSConstants {

	public static final String COMPONENT_USER = "User";
	public static final String COMPONENT_APPLICATION = "Application";
	public static final String COMPONENT_ORGANIZATION = "Organization";
	public static final String COMPONENT_ROLES = "Role";
	public static final String COMPONENT_APPLICATION_INSTANCE = "ApplicationInstance";
	public static final String COMPONENT_SCHEDULER = "Scheduler";
	public static final String COMPONENT_EMAIL = "Email";
	public static final String COMPONENT_SYSTEM_PROPERTY = "SystemProperty";
	public static final String COMPONENT_WORKFLOW = "Workflow";
	public static final String COMPONENT_USER_ATTRIBUTES = "UserAttributes";
	public static final String COMPONENT_ACCESS_POLICY = "AccessPolicy";
	public static final String COMPONENT_ROLE_MEMBERSHIP_RULE = "RoleMembershipRule";
	public static final String COMPONENT_APPLICATION_PROFILE = "ApplicationProfile";
	public static final String COMPONENT_ACCESS_PROFILE = "AccessProfile";

}
