package org.cps.web.ui.beans;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.idx.application.dob.ApplicationProfileDOB;
import org.idx.application.dob.ApplicationProfileMappingDOB;
import org.idx.prov.accesspolicy.dob.ORG_ATT_RULE_CONDITION;
import org.idx.prov.accesspolicy.dob.OrgAttributeConditionDOB;
import org.idx.prov.accesspolicy.dob.USER_ATT_RULE_CONDITION;
import org.idx.prov.accesspolicy.dob.USER_RULE_APPEND_CONDITION;
import org.idx.prov.accesspolicy.dob.UserAttributeConditionDOB;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DualListModel;
import org.springframework.stereotype.Component;
 
import lombok.Data;

@Component
@Data
public class APWizardBean implements Serializable {
	
	private String name;
	private String description;
	private String apType;
	private boolean isRoleBased = true;
	private boolean isUserBased;
	private boolean isOrgBased;

	List<UserAttributeConditionDOB> userAttConditions = new ArrayList<UserAttributeConditionDOB>();
	private List<SelectItem> andOrRuleConditions = new ArrayList<SelectItem>();
	private List<SelectItem> andOrRuleConditions2 = new ArrayList<SelectItem>();
	private List<SelectItem> userAttRuleConditions = new ArrayList<SelectItem>();
	private List<SelectItem> userAttributeList = new ArrayList<SelectItem>();
	private List<String> availableRoles = new ArrayList<String>();
	private List<String> selectedRoles = new ArrayList<String>();
	private DualListModel<String> roles;
	private List<OrgAttributeConditionDOB> orgAttConditions = new ArrayList<OrgAttributeConditionDOB>();;
	
	private List<SelectItem> orgAttributeList = new ArrayList<SelectItem>();
	private List<SelectItem> orgAttRuleConditions = new ArrayList<SelectItem>();
	
	private List<String> availableApps = new ArrayList<String>();
	private List<String> selectedApps = new ArrayList<String>();
	private DualListModel<String> apps;

	private boolean withApproval = false;
	
	
	private List<ApplicationProfileMappingDOB> appProfileMapping;
	
	
	private boolean skip;

	public APWizardBean() {
		System.out.println("######### Acess Policy Wizard Bean #########");
	}

	@PostConstruct
	public void init() {
		System.out.println("######### init #########");
		
		SelectItem att1 = new SelectItem();
		att1.setLabel("Organization");
		att1.setValue("Org");

		SelectItem att2 = new SelectItem();
		att2.setLabel("Coalition");
		att2.setValue("coalition");

		SelectItem att3 = new SelectItem();
		att3.setLabel("Department");
		att3.setValue("department");
		
		SelectItem att4 = new SelectItem();
		att4.setLabel("Employee Type");
		att4.setValue("EmpType");
		
		SelectItem att5 = new SelectItem();
		att5.setLabel("Manager");
		att5.setValue("Manager");
		
		userAttributeList.add(att1);
		userAttributeList.add(att2);
		userAttributeList.add(att3);
		userAttributeList.add(att4);
		userAttributeList.add(att5);

		
		SelectItem andOr1 = new SelectItem();
		andOr1.setLabel("AND");
		andOr1.setValue(USER_RULE_APPEND_CONDITION.AND);

		andOrRuleConditions.add(andOr1);

		SelectItem andOr2 = new SelectItem();
		andOr2.setLabel("AND");
		andOr2.setValue(USER_RULE_APPEND_CONDITION.AND);

		andOrRuleConditions2.add(andOr1);

		SelectItem attRul1 = new SelectItem();
		attRul1.setLabel("Equals");
		attRul1.setValue(USER_ATT_RULE_CONDITION.EQUALS);

		SelectItem attRul2 = new SelectItem();
		attRul2.setLabel("Not Equals");
		attRul2.setValue(USER_ATT_RULE_CONDITION.NOT_EQUALS);
		
		userAttRuleConditions.add(attRul1);
		userAttRuleConditions.add(attRul2);

		SelectItem attRul3 = new SelectItem();
		attRul3.setLabel("Equals");
		attRul3.setValue(ORG_ATT_RULE_CONDITION.EQUALS);

		SelectItem attRul4 = new SelectItem();
		attRul4.setLabel("Not Equals");
		attRul4.setValue(ORG_ATT_RULE_CONDITION.NOT_EQUALS);

		orgAttRuleConditions.add(attRul3);
		orgAttRuleConditions.add(attRul4);

		SelectItem orgAtt1 = new SelectItem();
		orgAtt1.setLabel("Organazation");
		orgAtt1.setValue("Organazation");

		SelectItem orgAtt2 = new SelectItem();
		orgAtt2.setLabel("Parent Organazation");
		orgAtt2.setValue("Parent Organazation");

		SelectItem orgAtt3 = new SelectItem();
		orgAtt3.setLabel("Child Organazation");
		orgAtt3.setValue("Child Organazation");

		orgAttributeList.add(orgAtt1);
		orgAttributeList.add(orgAtt2);
		orgAttributeList.add(orgAtt3);

 
		
		

		availableRoles.add("HRPB Group");
		availableRoles.add("Infrasture Support");
		availableRoles.add("IT Development");
		availableRoles.add("IT QA");
		availableRoles.add("IT Support");
		availableRoles.add("Admin");
		availableRoles.add("Help Desk");

		roles = new DualListModel<String>(availableRoles, selectedRoles);
		
		
		
		availableApps.add("Active Directory");
		availableApps.add("IT Infra Directory");
		availableApps.add("Office 365");
		availableApps.add("Google Apps");
		availableApps.add("Linux");
		availableApps.add("HR DB");
		availableApps.add("Applications DB");
		availableApps.add("Badge");
		
		apps = new DualListModel<String>(availableApps, selectedApps);
		
		
		appProfileMapping = new ArrayList<ApplicationProfileMappingDOB>();
		
		ApplicationProfileMappingDOB appProfileMapping1 = new ApplicationProfileMappingDOB();
		appProfileMapping1.setApplicationName("AD");
		ApplicationProfileDOB appProfile1 = new ApplicationProfileDOB();
		appProfile1.setName("AD Profile HR");

		ApplicationProfileDOB appProfile2 = new ApplicationProfileDOB();
		appProfile2.setName("AD Profile IT");

		ApplicationProfileDOB appProfile3 = new ApplicationProfileDOB();
		appProfile3.setName("AD Profile Admin");

		ApplicationProfileDOB appProfile4 = new ApplicationProfileDOB();
		appProfile4.setName("Employee");

		ApplicationProfileDOB appProfile5 = new ApplicationProfileDOB();
		appProfile5.setName("Vendor");
		
		List<ApplicationProfileDOB> appProfiles = new ArrayList<ApplicationProfileDOB>();
		appProfiles.add(appProfile1);
		appProfiles.add(appProfile2);
		appProfiles.add(appProfile3);
		appProfiles.add(appProfile4);
		appProfiles.add(appProfile5);

		appProfileMapping.add(appProfileMapping1);

		
		appProfileMapping1.setProfiles(appProfiles);
 	}

	public void save() {

	}

	
 	
 	public boolean isSkip() {
		return skip;
	}

	public void setSkip(boolean skip) {
		this.skip = skip;
	}

	public String onFlowProcess(FlowEvent event) {

		System.out.println("onFlowProcess called : " + event.getOldStep());
		System.out.println("onFlowProcess called : " + event.getNewStep());
		System.out.println("onFlowProcess called : " + event.getSource());
		
		System.out.println("######### apType ######### " + apType);

		if(apType.equals("role")) {
			isRoleBased = true;
			isUserBased=false;
			isOrgBased=false;

		}
		else if(apType.equals("user")) {
			isRoleBased = false;
			isUserBased=true;
			isOrgBased=false;

		}
		else if(apType.equals("org")) {
			isRoleBased = false;
			isUserBased=false;
			isOrgBased=true;

		}		

		
		if (skip) {
			skip = false; // reset in case user goes back
			return "confirm";
		} else {
			return event.getNewStep();
		}
	}

	public void removeUserAttConditionRow(UserAttributeConditionDOB userAttCondition){
		userAttConditions.remove(userAttCondition);
		
	}
	public void onSelectPolicyType(){
		System.out.println("######### apType ######### " + apType);

	}

	public void addUserAttributeCondition(){
		System.out.println("######### apType ######### " + apType);
		
		
		UserAttributeConditionDOB attCondition = new UserAttributeConditionDOB();
		userAttConditions.add(attCondition);

	}
	
	public void addOrgAttributeCondition(){
		OrgAttributeConditionDOB attCondition = new OrgAttributeConditionDOB();
		orgAttConditions .add(attCondition);

	}

	public void removeOrgAttributeCondition(OrgAttributeConditionDOB attCondition){
		orgAttConditions.remove(attCondition);
	}

	
	

	public void updateValue() {
		System.out.println("######### Farzi Method ######### ");

	}
	
    public void onSelect(SelectEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Item Selected", event.getObject().toString()));
    }
     
    public void onUnselect(UnselectEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Item Unselected", event.getObject().toString()));
    }


 
 }
