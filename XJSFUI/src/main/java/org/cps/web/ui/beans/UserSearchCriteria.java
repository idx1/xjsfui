package org.cps.web.ui.beans;

import java.util.List;

import javax.annotation.PostConstruct;

import org.cps.framework.common.SearchCriteria;
import org.cps.web.ui.model.UserVO;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class UserSearchCriteria extends SearchCriteria{
	
	public UserSearchCriteria() {
		System.out.println("############## UserSearchCriteria ##################");
	}
	

	List<UserVO> searchResult;
	
	UserVO selectedUser;
	
	public String option = "Any";
	
	public String uNameCondition;

	public String userName;

	public String fNameCondition;

	public String firstName;

	public String mNameCondition;

	public String middleName;

	public String lNameCondition;

	public String lastName;

	public String dNameCondition;

	public String displayName;

	public String uTypeCondition;

	public String userType;

	public String uStatusCondition;

	public String userStatus;

	public String emailCondition;

	public String email;
	
	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public String getuNameCondition() {
		return uNameCondition;
	}

	public void setuNameCondition(String uNameCondition) {
		System.out.println("uNameCondition : " + uNameCondition);
		this.uNameCondition = uNameCondition;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		System.out.println("userName : " + userName);
		this.userName = userName;
	}

	public String getfNameCondition() {
		return fNameCondition;
	}

	public void setfNameCondition(String fNameCondition) {
		this.fNameCondition = fNameCondition;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getmNameCondition() {
		return mNameCondition;
	}

	public void setmNameCondition(String mNameCondition) {
		this.mNameCondition = mNameCondition;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getlNameCondition() {
		return lNameCondition;
	}

	public void setlNameCondition(String lNameCondition) {
		this.lNameCondition = lNameCondition;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getdNameCondition() {
		return dNameCondition;
	}

	public void setdNameCondition(String dNameCondition) {
		this.dNameCondition = dNameCondition;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getuTypeCondition() {
		return uTypeCondition;
	}

	public void setuTypeCondition(String uTypeCondition) {
		this.uTypeCondition = uTypeCondition;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getuStatusCondition() {
		return uStatusCondition;
	}

	public void setuStatusCondition(String uStatusCondition) {
		this.uStatusCondition = uStatusCondition;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getEmailCondition() {
		return emailCondition;
	}

	public void setEmailCondition(String emailCondition) {
		this.emailCondition = emailCondition;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<UserVO> getSearchResult() {
		return searchResult;
	}

	public void setSearchResult(List<UserVO> searchResult) {
		this.searchResult = searchResult;
	}

	public UserVO getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(UserVO selectedUser) {
		this.selectedUser = selectedUser;
	}
	
	
	
	

}
