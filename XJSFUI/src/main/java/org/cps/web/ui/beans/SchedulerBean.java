package org.cps.web.ui.beans;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.idx.application.dob.ApplicationDOB;
import org.idx.application.dob.ConfiguratorDOB;
import org.idx.common.rest.ServerResponse;
import org.idx.scheduler.dob.JOB_REPEATINTERVAL_TYPE;
import org.idx.scheduler.dob.ScheduledJobDOB;
import org.idx.scheduler.dob.ScheduledJobParamsDOB;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.Data;

@Data
@Component
@Scope("view")
public class SchedulerBean {
	

	private static final String SCH_POST_CREATE_JOB = "/createJob";

	private static final String SCHEDULER_REST_URL = "http://localhost:7080/scheduler";

	private static final String SCH_GET_ALL_JOBS = "/allJobs";

	private static final String SCH_GET_JOB_DETAILS = "/jobDetail";

	ScheduledJobDOB job;
	
	private List<SelectItem> intervalTypes;
	
	private List<ScheduledJobDOB> jobList;
	
	private ScheduledJobDOB selectedJob;

		
	@PostConstruct
	public void init() {
		System.out.println("############# init SchedulerBean ################ ");

		job = new ScheduledJobDOB();
		job.setJobType(1);
		
		intervalTypes = new ArrayList<SelectItem>();
		
		SelectItem  mins = new SelectItem();
		mins.setLabel("Minutes");
		mins.setValue(JOB_REPEATINTERVAL_TYPE.MINUTES);
		
		SelectItem  hours = new SelectItem();
		hours.setLabel("Hours");
		hours.setValue(JOB_REPEATINTERVAL_TYPE.HOURS);
		
		SelectItem  days = new SelectItem();
		days.setLabel("Days");
		days.setValue(JOB_REPEATINTERVAL_TYPE.DAYS);
		
		intervalTypes.add(mins);
		intervalTypes.add(hours);
		intervalTypes.add(days);
		
	}
	
	public void setSelectedJob(ScheduledJobDOB job) {
		
		System.out.println("select Job : "+ job);
		
	}
	
	public List<ScheduledJobDOB> getJobList(){
		
		HttpHeaders headers = new HttpHeaders();
	      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	      headers.setContentType(MediaType.APPLICATION_JSON);
	 
	      RestTemplate restTemplate = new RestTemplate();
	 
	      // Data attached to the request.
	      HttpEntity<ServerResponse> requestBody = new HttpEntity<ServerResponse>(headers);
	 

	      URI reqURI = null;
	      ResponseEntity<ScheduledJobDOB>  result = null;
		try {
			reqURI = new URI(SCHEDULER_REST_URL + SCH_GET_ALL_JOBS);
//		    result = restTemplate.postForEntity(reqURI, requestBody, ApplicationDOB.class);
		    
		    
		    ResponseEntity<ServerResponse> response = restTemplate.exchange(reqURI, HttpMethod.GET, requestBody, ServerResponse.class);

		    ServerResponse result1 = response.getBody();
		    
		    
		    jobList = (List<ScheduledJobDOB>) result1.getData();
 	    
		      System.out.println("Status code:" + response.getStatusCode());
		      System.out.println("Body:" + result1.getData());

		} catch (Exception e1) {
		      System.out.println("Body:" + e1.getMessage());
		      System.out.println("Body:" + e1.getCause());

			e1.printStackTrace();
		}

		
		
		return jobList;
		
	}
	

	public void addJobParameter(){
		ScheduledJobParamsDOB param = new ScheduledJobParamsDOB();
		job.getJobParams().add(param );
	}
	
	public void removeJobParameter(ScheduledJobParamsDOB param){
		job.getJobParams().remove(param);
	}

	
	
	public void updateValue() {
	}

	public void save() {
		System.out.println("Save : " + job);
		
		postRequest(job);
		
		FacesMessage msg = new FacesMessage("Successful", "Scheduled Job :" + job.getName() + " created successfully");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	public ResponseEntity<ScheduledJobDOB> postRequest(ScheduledJobDOB job) {
		
		
		HttpHeaders headers = new HttpHeaders();
	      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	      headers.setContentType(MediaType.APPLICATION_JSON);
	 
	      RestTemplate restTemplate = new RestTemplate();
	 
	      
		// Data attached to the request.
	      HttpEntity<ScheduledJobDOB> requestBody = new HttpEntity<ScheduledJobDOB>(job , headers);
	 
	      URI reqURI = null;
	      ResponseEntity<ScheduledJobDOB>  result = null;
		try {
			reqURI = new URI(SCHEDULER_REST_URL + SCH_POST_CREATE_JOB);
		    result = restTemplate.postForEntity(reqURI, requestBody, ScheduledJobDOB.class);

		} catch (Exception e1) {
		      System.out.println("Body:" + e1.getMessage());
		      System.out.println("Body:" + e1.getCause());

			e1.printStackTrace();
		}
 	      
	      if (result.getStatusCode() == HttpStatus.OK) {
	    	  ScheduledJobDOB e = result.getBody();
	    	  	System.out.println("(Client Side) Application Type Created: "+ e.getName());
	      }


		return result;
		
	}

	public void getJobDetails(String jobName) {

		HttpHeaders headers = new HttpHeaders();
	      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	      headers.setContentType(MediaType.APPLICATION_JSON);
	 
	      RestTemplate restTemplate = new RestTemplate();
	 
	      // Data attached to the request.
	      HttpEntity<ServerResponse> requestBody = new HttpEntity<ServerResponse>(headers);
	 

	      URI reqURI = null;
	      ResponseEntity<ScheduledJobDOB>  result = null;
		try {
			
			
			String url = SCHEDULER_REST_URL + SCH_GET_JOB_DETAILS;
			
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url )
			        .queryParam("jobName", jobName);

			
			
			reqURI = builder.build().encode().toUri();
			
//		    result = restTemplate.postForEntity(reqURI, requestBody, ApplicationDOB.class);
		    
		    
		    ResponseEntity<ServerResponse> response = restTemplate.exchange(reqURI, HttpMethod.GET, requestBody, ServerResponse.class);

		    ServerResponse result1 = response.getBody();
		    
		    System.out.println(result1);
		    
		    ScheduledJobDOB jobDetail = (ScheduledJobDOB) result1.getData();
	    
		      System.out.println("Status code:" + response.getStatusCode());
		      System.out.println("jobDetail:" + jobDetail);

		} catch (Exception e1) {
		      System.out.println("Body:" + e1.getMessage());
		      System.out.println("Body:" + e1.getCause());

			e1.printStackTrace();
		}

		
		
		
	}




}
