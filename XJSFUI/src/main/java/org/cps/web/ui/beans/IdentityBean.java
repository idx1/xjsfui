package org.cps.web.ui.beans;


import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.cps.identity.exception.UserManagerException;
import org.cps.web.client.IdentityClientService;
import org.cps.web.exception.UserCreationException;
import org.cps.web.ui.model.LookupBean;
import org.cps.web.ui.model.RoleVO;
import org.cps.web.ui.model.UserVO;
import org.cps.web.utils.WebConstants;
import org.cps.web.utils.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class IdentityBean {
	
	@Autowired
	IdentityClientService identityService;

	private LookupBean orgLookup;

	public LookupBean getOrgLookup() {
		return orgLookup;
	}



	public void setOrgLookup(LookupBean orgLookup) {
		this.orgLookup = orgLookup;
	}
	
	public void createRole(RoleVO roleVO) {
		
	}

	public void createUser(UserVO userVO) {
		
		String response = null;
		
		try {
			response = identityService.createUser(userVO);
		} catch (Throwable e) {
			
			System.out.println("###########################################################################");
			
			e.printStackTrace();
			
	        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Problem While Creating User", e.getMessage());
	        FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		if(response !=null) {
			closeCurrentTab();
	        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "User Created Successfully", "");
	        FacesContext.getCurrentInstance().addMessage(null, message);

		}

		
	}
	
	public void searchUser(UserSearchCriteria searchCriteria) throws UserManagerException {
		System.out.println("## in search User ##");
		
		
		String userName = searchCriteria.getUserName();
		String firstName = searchCriteria.getFirstName();
		String middleName = searchCriteria.getMiddleName();
		String lastName = searchCriteria.getLastName();
		String email = searchCriteria.getEmail();
		
		String uNameCondition = searchCriteria.getuNameCondition();
		String fNameCondition = searchCriteria.getfNameCondition();
		String mNameCondition = searchCriteria.getmNameCondition();
		String lNameCondition = searchCriteria.getlNameCondition();
		String emailCondition = searchCriteria.getEmailCondition();
		
		System.out.println("userName : " + userName);
		System.out.println("firstName : " + firstName);
		System.out.println("middleName : " + middleName);
		System.out.println("lastName : " + lastName);
		System.out.println("email : " + email);
		
		String BLANK = "";
		
		if((userName==null || userName.trim().equals(BLANK)) &&
				(firstName==null || userName.trim().equals(BLANK)) &&
				(middleName==null || middleName.trim().equals(BLANK)) &&
				(lastName==null || lastName.trim().equals(BLANK)) &&
				(email==null || email.trim().equals(BLANK))) {
			
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Blank Search not Allowed, Please specify Search Criteria", null);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}

		
		List<UserVO> users = identityService.searchUser(searchCriteria);
		
		System.out.println("users : "+ users);
		
		searchCriteria.setSearchResult(users);
	}


	private void closeCurrentTab() {
		
		AdminBean adminBean = (AdminBean) WebUtils.getBeanInstance("#{adminBean}", AdminBean.class);

		System.out.println("adminBean : " + adminBean);
		
		adminBean.closeCurrentTab();
		
	}
}
