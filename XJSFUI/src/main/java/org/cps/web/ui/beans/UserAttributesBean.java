package org.cps.web.ui.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;
import org.cps.web.client.IdentityClientService;
import org.idx.identity.dob.USER_ATTR_TYPE;
import org.idx.identity.dob.UserAttributeDOB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@Scope("session")
public class UserAttributesBean implements Serializable {
	
	@Autowired
	private IdentityClientService identityService;
	
	private List<UserAttributeDOB> userAttributes;
	
	private UserAttributeDOB userAttr = new UserAttributeDOB();

	private List<SelectItem> userAttrTypes;
	
	
	
	@PostConstruct
	public void init() {
		System.out.println("######### init UserAttributesBean #########");	
		userAttributes = identityService.getUserAttributes();
		
		userAttrTypes = new ArrayList<SelectItem>();
		
		SelectItem userAttrType1 = new SelectItem();
		
		userAttrType1.setLabel("String");
		userAttrType1.setValue(USER_ATTR_TYPE.STRING);
		
		userAttrTypes.add(userAttrType1);

		SelectItem userAttrType2 = new SelectItem();

		userAttrType2.setLabel("String");
		userAttrType2.setValue(USER_ATTR_TYPE.SECRET);
		
		userAttrTypes.add(userAttrType2);

		SelectItem userAttrType3 = new SelectItem();

		userAttrType3.setLabel("Date");
		userAttrType3.setValue(USER_ATTR_TYPE.DATE);
		
		userAttrTypes.add(userAttrType3);

		SelectItem userAttrType4 = new SelectItem();

		userAttrType4.setLabel("Boolean");
		userAttrType4.setValue(USER_ATTR_TYPE.BOOLEAN);
		
		userAttrTypes.add(userAttrType4);

		SelectItem userAttrType5 = new SelectItem();

		userAttrType5.setLabel("Number");
		userAttrType5.setValue(USER_ATTR_TYPE.NUMBER);
		
		userAttrTypes.add(userAttrType5);

	}
	
	
	
	
	public void openCreateAttribute(){
		System.out.println("######### openCreateAttribute UserAttributesBean #########");	

		Map<String,Object> options = new HashMap<String, Object>();
		options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", true);
		PrimeFaces.current().dialog().openDynamic("createUserAttribute", options, null);
		
		
//		RequestContext context2 = RequestContext.getCurrentInstance();
//		  context2.execute("PF('createUserAttribute').show()");

	}
	
	public void createUserAttributes(){
		
	}
	
}
