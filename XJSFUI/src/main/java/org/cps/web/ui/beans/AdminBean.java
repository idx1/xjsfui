package org.cps.web.ui.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.SystemEvent;
import javax.faces.event.SystemEventListener;

import org.cps.web.common.CPSConstants;
import org.idx.scheduler.dob.ScheduledJobDOB;
import org.omnifaces.util.Components;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.TabCloseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;

//@ManagedBean(name = "adminBean")
//@SessionScoped
@Component
@Scope("session")
public class AdminBean implements SystemEventListener{
	
	private String tabId = "AdminTab";
	private TabView tabView;


	@Autowired
	SchedulerBean schedulerBean;
	
	public AdminBean(){
			      
		loggedInUser = (String)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		
	     System.out.println("_________________@@@@@@@@@@@@ ________________"+ SecurityContextHolder.getContext().getAuthentication().getPrincipal().getClass());
	     System.out.println("_________________@@@@@@@@@@@@ ________________"+ SecurityContextHolder.getContext().getAuthentication().getPrincipal());

	}
	
	private String loggedInUser;

	public String getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	
	
	public void createUser() {
		createNewTab(CPSConstants.COMPONENT_USER, true, false, false , false);
	}

	public void searchUser() {
		createNewTab(CPSConstants.COMPONENT_USER, false, false, false, true);
	}

	public void createRole() {
		createNewTab(CPSConstants.COMPONENT_ROLES, true, false, false , false);
	}

	public void searchRole() {
		createNewTab(CPSConstants.COMPONENT_ROLES, false, false, false, true);
	}

	public void createOrg() {
		createNewTab(CPSConstants.COMPONENT_ORGANIZATION, true, false, false , false);
	}

	public void searchOrg() {
		createNewTab(CPSConstants.COMPONENT_ORGANIZATION, false, false, false, true);
	}

	public void createApplication() {
		createNewTab(CPSConstants.COMPONENT_APPLICATION, true, false, false , false);
	}

	public void searchApplication() {
		createNewTab(CPSConstants.COMPONENT_APPLICATION, false, false, false, true);
	}

	
	public void createApplicationProfile(){
		createNewTab(CPSConstants.COMPONENT_APPLICATION_PROFILE, true, false, false , false);
	}
	public void searchApplicationProfile(){
		createNewTab(CPSConstants.COMPONENT_APPLICATION_PROFILE, false, false, false, true);
	}

	public void createAccessProfile(){
		createNewTab(CPSConstants.COMPONENT_ACCESS_PROFILE, true, false, false , false);
	}
	public void searchAccessProfile(){
		createNewTab(CPSConstants.COMPONENT_ACCESS_PROFILE, false, false, false, true);
	}

	
	public void createSchedulerJob() {
		createNewTab(CPSConstants.COMPONENT_SCHEDULER, true, false, false , false);
	}

	public void searchSchedulerJob() {
		createNewTab(CPSConstants.COMPONENT_SCHEDULER, false, false, false, true);
	}

	public void createEmailTemplate() {
		createNewTab(CPSConstants.COMPONENT_EMAIL, true, false, false , false);
	}

	public void searchEmailTemplate() {
		createNewTab(CPSConstants.COMPONENT_EMAIL, false, false, false, true);
	}

	public void createSystemProperties() {
		createNewTab(CPSConstants.COMPONENT_SYSTEM_PROPERTY, true, false, false , false);
	}

	public void searchSystemProperties() {
		createNewTab(CPSConstants.COMPONENT_SYSTEM_PROPERTY, false, false, false, true);
	}

	public void createWorkflows() {
		createNewTab(CPSConstants.COMPONENT_WORKFLOW, true, false, false , false);
	}

	public void searchWorkflows() {
		createNewTab(CPSConstants.COMPONENT_WORKFLOW, false, false, false, true);
	}

	public void userAttributesAdministration(){
		createNewTab(CPSConstants.COMPONENT_USER_ATTRIBUTES, false, false, false, true);
	}

	public void createUserAttributes(){
		createNewTab(CPSConstants.COMPONENT_USER_ATTRIBUTES, true, false, false, false);
	}

	public void createAccessPolicies(){
		createNewTab(CPSConstants.COMPONENT_ACCESS_POLICY, true, false, false , false);
	}

	public void searchAccessPolicies(){
		createNewTab(CPSConstants.COMPONENT_ACCESS_POLICY, false, false, false, true);
	}

	public void createRoleMembershipRule(){
		createNewTab(CPSConstants.COMPONENT_ROLE_MEMBERSHIP_RULE, true, false, false , false);
	}

	public void searchRoleMembershipRule(){
		createNewTab(CPSConstants.COMPONENT_ROLE_MEMBERSHIP_RULE, false, false, false, true);
	}
	
	
	
	public void showSchduledJob(String jobName){
		
		
		schedulerBean.getJobDetails(jobName);
		createNewTab(CPSConstants.COMPONENT_SCHEDULER, jobName, jobName, false, true, false, false);
		
		
	}
	
	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public TabView getTabView() {
		return tabView;
	}

	public void setTabView(TabView tabView) {
		this.tabView = tabView;
	}

	public void createNewTab(String component, boolean newComponent, boolean viewComponent, boolean editComponent, boolean searchComponent) {
		
		String tabId = null;
		
		if(newComponent)
			tabId="new"+component;
		else if(viewComponent)
			tabId="view"+component;
		else if(editComponent)
			tabId="edit"+component;
		else if(searchComponent)
			tabId="search"+component;
		
		createNewTab(component, component, tabId, newComponent, viewComponent, editComponent, searchComponent);
	}
	
	public void createNewTab(String component, String title, String tabId, boolean newComponent, boolean viewComponent, boolean editComponent, boolean searchComponent) {
		
		Tab newTab = new Tab(); // Create new Tab
		newTab.setId(tabId);
		
 		
		newTab.setTitle(title); // Set a basic property
		newTab.setClosable(true);
		 
		try {
			FacesContext ctx = FacesContext.getCurrentInstance();
			String url = null;
			
			if(component.equals(CPSConstants.COMPONENT_USER) && newComponent)
				url = "../pages/createUser.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_USER) && searchComponent)
				url = "../pages/searchUser.xhtml";
			if(component.equals(CPSConstants.COMPONENT_ROLES) && newComponent)
				url = "../pages/createRole.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_ROLES) && searchComponent)
				url = "../pages/searchRole.xhtml";
			if(component.equals(CPSConstants.COMPONENT_ORGANIZATION) && newComponent)
				url = "../pages/createOrg.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_ORGANIZATION) && searchComponent)
				url = "../pages/searchOrg.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_APPLICATION) && newComponent)
				url = "../pages/createApplication.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_APPLICATION) && searchComponent)
				url = "../pages/searchApplication.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_SCHEDULER) && newComponent)
				url = "../pages/createJob.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_SCHEDULER) && searchComponent)
				url = "../pages/searchJob.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_SCHEDULER) && viewComponent)
				url = "../pages/jobDetail.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_EMAIL) && newComponent)
				url = "../pages/createEmailTemplate.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_EMAIL) && viewComponent)
				url = "../pages/searchEmailTemplate.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_SYSTEM_PROPERTY) && newComponent)
				url = "../pages/createSystemProperty.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_SYSTEM_PROPERTY) && searchComponent)
				url = "../pages/searchSystemProperty.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_WORKFLOW) && newComponent)
				url = "../pages/createWorkflow.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_WORKFLOW) && searchComponent)
				url = "../pages/searchWorkflow.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_USER_ATTRIBUTES) && searchComponent)
				url = "../pages/userAttributes.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_USER_ATTRIBUTES) && newComponent)
				url = "../pages/createUserAttribute.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_ACCESS_POLICY) && newComponent)
				url = "../pages/createAccessPlolicy.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_ACCESS_POLICY) && searchComponent)
				url = "../pages/searchAccessPolicy.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_ROLE_MEMBERSHIP_RULE) && newComponent)
				url = "../pages/createRoleMembershipRule.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_ROLE_MEMBERSHIP_RULE) && searchComponent)
				url = "../pages/searchRoleMembershipRule.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_APPLICATION_PROFILE) && newComponent)
				url = "../pages/createApplicationProfile.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_APPLICATION_PROFILE) && searchComponent)
				url = "../pages/searchApplicationProfile.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_ACCESS_PROFILE) && newComponent)
				url = "../pages/createAccessProfile.xhtml";
			else if(component.equals(CPSConstants.COMPONENT_ACCESS_PROFILE) && searchComponent)
				url = "../pages/searchAccessProfile.xhtml";
			
			


			System.out.println(url);
			
			Components.includeFacelet(newTab, url);
			
	 		tabView = (TabView) ctx.getViewRoot().findComponent("adminTabPanelView:adminTabPanel");
			int tabCount = tabView.getChildCount();
	 		tabView.getChildren().add(tabCount, newTab);
	 		tabView.setActiveIndex(tabView.getChildCount() - 1);
			RequestContext context = RequestContext.getCurrentInstance();
			context.update("adminTabPanelView:adminTabPanel");
//			context.addCallbackParam("newTabIndex", lastTabIndex);
			

			
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		
	}
	
	public void onTabClose(TabCloseEvent event) {
		System.out.println("onTabClose called : "+event);

		System.out.println("onTabClose id : "+event.getTab().getClientId());
		
		Tab closedTab = event.getTab();

		Tab changedTab = event.getTab();
		UIComponent tabToRemove = null;
		List<UIComponent> tabs = tabView.getChildren();

		int index=0;
		for(UIComponent tab:tabs){
			if(tab.getId().equals(changedTab.getId())){
				tabToRemove = tab;
			}
			index++;
		}
		boolean b = tabs.remove(tabToRemove);

		
	}

	public void onTabChange(TabChangeEvent event) {
		System.out.println("onTabClose called : "+event);
	}

	@Override
	public boolean isListenerForSource(Object arg0) {
		System.out.println("isListenerForSource called : "+arg0);
		return false;
	}

	@Override
	public void processEvent(SystemEvent event) throws AbortProcessingException {
		System.out.println("processEvent called : "+event);
		
	}

	public void closeCurrentTab() {
 		
		FacesContext ctx = FacesContext.getCurrentInstance();

		tabView = (TabView) ctx.getViewRoot().findComponent("adminTabPanelView:adminTabPanel");

		List<UIComponent> tabs = tabView.getChildren();

		int tabIndex = tabView.getActiveIndex();

		
		Tab currentTab = getCurrentTab();

		currentTab.setRendered(false);

		boolean b = tabs.remove(currentTab);

		System.out.println("remove : "+ b);
		
		tabView.setActiveIndex(tabIndex -1);

		RequestContext context = RequestContext.getCurrentInstance();
		context.update("adminTabPanelView:adminTabPanel");
		
	}
	
	private Tab getCurrentTab(){
		List<UIComponent> tabs = tabView.getChildren();
		return (Tab)tabs.get(tabView.getActiveIndex());
	}
	
}
