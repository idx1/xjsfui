package org.cps.web.ui.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.cps.web.client.ApplicationClientService;
import org.idx.application.dob.ApplicationDOB;
import org.idx.application.dob.ApplicationProfileDOB;
import org.idx.application.dob.EntitlementDOB;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class ApplicationProfileWizardBean {
	
	private String name;
	private String displayName;
	private String description;
	private ApplicationDOB application1;
	private String application;
	
	private List<SelectItem> applicationList;
	
//	private List<EntitlementDOB> availableEntitlements = new ArrayList<EntitlementDOB>();
//	private List<EntitlementDOB> selectedEntitlements = new ArrayList<EntitlementDOB>();
//	private DualListModel<EntitlementDOB> entitlements;

	private List<EntitlementDOB> entitlements;
	private List<EntitlementDOB> selectedEntitlements;
	
	@Autowired
	ApplicationClientService applicationService;

	
	@PostConstruct
	public void init() {
		System.out.println("######### init #########");
		
		ApplicationDOB[] apps= applicationService.getApplicationList();
		
		applicationList = new ArrayList<SelectItem>();
		
		for(ApplicationDOB app:apps) {
		
			SelectItem item1 = new SelectItem();
			item1.setLabel(app.getDisplayName());
			item1.setValue(app.getName());

			applicationList.add(item1);

		}
		

		
	}

 	
	public String onFlowProcess(FlowEvent event) {

		System.out.println("odl step : " + event.getOldStep());
		System.out.println("nextStep  : " + event.getNewStep());
		System.out.println("onFlowProcess called : " + event.getSource());
		System.out.println("application : " + application);
		
		System.out.println("ye hai selectedEntitlements : " + selectedEntitlements);

		
		String nextStep = event.getNewStep();
		
		if(entitlements == null && nextStep.equals("ent")) {
			
			System.out.println("ye hai entitlements : " + entitlements);
			System.out.println("ye hai applicationList : " + applicationList);
			
			
			EntitlementDOB[] ents = applicationService.getEntitlementsForApplication(application);
			
			//System.out.println("ents : " + ents);

			
			entitlements = new ArrayList<EntitlementDOB>();
			
			for(EntitlementDOB ent: ents) {
				entitlements.add(ent);
			}
			
 
		}
		
		return event.getNewStep();
	}
	
	public void save() {
		
		ApplicationProfileDOB profile = new ApplicationProfileDOB();
		
		profile.setName(name);
		profile.setDescription(description);
		profile.setDisplayName(displayName);
		
		profile.setEntitlements(selectedEntitlements);
		
		System.out.println("application : "+ application);
		
		ApplicationDOB app = applicationService.getApplication(application);
		System.out.println("app : "+ app);

		profile.setApplication(app);
		
		boolean savedSuccessfully = true;
		
		try {
			applicationService.createApplicationProfile(profile);
		} catch (Exception e) {
			savedSuccessfully = false;
			e.printStackTrace();
		}
		
		System.out.println("profile : "+ profile);
		
        FacesContext context = FacesContext.getCurrentInstance();
        
        if(savedSuccessfully) {       
        	context.getCurrentInstance().addMessage("adminTabPanelView:adminTabPanel", new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Application Profile : " + profile.getName() + "Successfully Created") );
        	closeCurrentTab();
        }
        else        
        	context.getCurrentInstance().addMessage("adminTabPanelView:adminTabPanel", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error!", "Problem while creating Application Profile"));
	}
	
	private void closeCurrentTab() {

		FacesContext ctx = FacesContext.getCurrentInstance();
 		TabView tabView = (TabView) ctx.getViewRoot().findComponent("adminTabPanelView:adminTabPanel");
		int tabCount = tabView.getChildCount();
		
 		List<UIComponent> childs = tabView.getChildren();
 		
 		Tab tabToClose = tabView.findTab("adminTabPanelView:adminTabPanel:newApplicationProfile");
 		
 		
 		
 		childs.remove(tabToClose);
 		
 		tabView.setActiveIndex(tabView.getActiveIndex() - 1);

		RequestContext context = RequestContext.getCurrentInstance();
		context.update("adminTabPanelView:adminTabPanel");

 		
 		
// 		for(UIComponent child: childs) {
// 			System.out.println("child : "+ child);
// 			System.out.println("child : "+ child.getId());
// 			System.out.println("child : "+ child.getClientId());
// 			
// 			UIComponent currentTab = (UIComponent)child;
// 
// 		}
		
	}


	public void updateValue() {
	}

	public void reset() {
		
	}
}
