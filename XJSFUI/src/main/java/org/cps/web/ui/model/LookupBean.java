package org.cps.web.ui.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

 
import org.cps.web.client.ComponentSearchService;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import flexjson.JSON;

@Component
@Scope("session")
public class LookupBean implements Serializable {

	/**
	 * 
	 */
    private static final long serialVersionUID = 1L;

	private String searchText;
	private List<Lookup> lookupResult;
	private LookupDataModel lookupDataModel;
	private Lookup selectedLookup;
	private String selectedName;
	private String selectedId;
	private String value;
	private Lookup valueHolder = new Lookup();
	private String component;
	
	@Autowired
	private ComponentSearchService searchService;
	
	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public ComponentSearchService getSearchService() {
		return searchService;
	}

	public void setSearchService(ComponentSearchService searchService) {
		this.searchService = searchService;
	}

	public LookupBean() {
		System.out.println("Khali");
	}

	public LookupBean(String component) {
		this.component = component;
		System.out.println("Bhara : " + component);
	}

	public Lookup getValueHolder() {
		return valueHolder;
	}

	public void setValueHolder(Lookup valueHolder) {
		this.valueHolder = valueHolder;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getSelectedName() {
		return selectedName;
	}

	public void setSelectedName(String selectedName) {
		this.selectedName = selectedName;
	}

	public String getSelectedId() {
		return selectedId;
	}

	public void setSelectedId(String selectedId) {
		this.selectedId = selectedId;
	}

	private boolean tableRendered = false;

	public boolean isTableRendered() {
		return tableRendered;
	}

	public void setTableRendered(boolean tableRendered) {
		this.tableRendered = tableRendered;
	}

	public Lookup getSelectedLookup() {
		return selectedLookup;
	}

	public void setSelectedLookup(Lookup selectedLookup) {
		
//		try{throw new Exception();}catch(Exception e){e.printStackTrace();}
		
		System.out.println("######## selectedLookup : " + selectedLookup);
		
		this.selectedLookup = selectedLookup;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String select(){
		selectedName=selectedLookup.getName();
		selectedId=selectedLookup.getId();

		valueHolder.setId(selectedId);
		valueHolder.setName(selectedName);

		return "selectResult";
	}

	public String search(){

		List<SearchResultData> searchResult = searchService.search(component, searchText);
		lookupResult = new ArrayList<Lookup>();

		for(SearchResultData searchData: searchResult){
			lookupResult.add(new Lookup(""+searchData.getId(), searchData.getName(), searchData.getDescription()));
		}

//		populateRandomCars(lookupResult);
		lookupDataModel = new LookupDataModel(lookupResult);
		tableRendered=true;
		return "searchResult";
	}

	@JSON(include=false)
	public List<Lookup> getLookupResult() {
		return lookupResult;
	}

	public void setLookupResult(List<Lookup> lookupResult) {
		this.lookupResult = lookupResult;
	}

	@JSON(include=false)
	public LookupDataModel getLookupDataModel() {
		return lookupDataModel;
	}

	public void setLookupDataModel(LookupDataModel lookupDataModel) {
		this.lookupDataModel = lookupDataModel;
	}

    public void onRowSelect(SelectEvent event) { 
    	Lookup lookup = (Lookup)event.getObject();
    	
    	System.out.println("lookup : "+ lookup.getName());
    	System.out.println("lookup : "+ lookup.getId());
    }  
  
    public void onRowUnselect(UnselectEvent event) {  
//        FacesMessage msg = new FacesMessage("Car Unselected", ((Car) event.getObject()).getModel());  
//  
//        FacesContext.getCurrentInstance().addMessage(null, msg);  
    }

	public void reset() {
//		searchText=null;
//		lookupResult=null;
//		lookupDataModel=null;
//		selectedLookup=null;
//		selectedName=null;
//		selectedId=null;
//		value=null;
//		component=null;

	}  

}