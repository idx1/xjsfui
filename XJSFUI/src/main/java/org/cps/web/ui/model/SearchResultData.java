package org.cps.web.ui.model;


import java.io.Serializable;

public class SearchResultData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String name;
	private String description;
	private String component;
	
	public SearchResultData(){
		
	}
	
	public SearchResultData(long id, String name, String description,
			String component) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.component = component;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	
	
	

}
