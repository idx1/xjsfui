package org.cps.web.ui.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class UserVO implements Serializable{
	
	
	public UserVO() {
		System.out.println("############## UserVO ##################");
	}

	private int userKey;
	private String firstName;
	private String middleName;
	private String lastName;
	private String displayName;
	private int userType;
	private long managerKey;
	private long orgKey;
	
//	private Lookup organization = new Lookup();//(HYSConstants.COMPONENT_ORGANIZATION);
//	
//	private Lookup manager = new Lookup();//HYSConstants.COMPONENT_USER);

	private String email;
	private String userName;
	private String password;
	private Date startDate;
	private Date endDate;
	private String phoneNumber;
	private String mobileNumber;
	private String postalAddress1;
	private String postalAddress2;
	private String postalCode;
	private String city;
	private String state;
	private String country;
	
	
	
	public int getUserKey() {
		return userKey;
	}
	public void setUserKey(int userKey) {
		this.userKey = userKey;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public int getUserType() {
		return userType;
	}
	public void setUserType(int userType) {
		this.userType = userType;
	}
	public long getManagerKey() {
		return managerKey;
	}
 
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getPostalAddress1() {
		return postalAddress1;
	}
	public void setPostalAddress1(String postalAddress1) {
		this.postalAddress1 = postalAddress1;
	}
	public String getPostalAddress2() {
		return postalAddress2;
	}
	public void setPostalAddress2(String postalAddress2) {
		this.postalAddress2 = postalAddress2;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
//	public Lookup getOrganization() {
//		return organization;
//	}
//	public void setOrganization(Lookup organization) {
//		this.organization = organization;
//	}
//	public Lookup getManager() {
//		return manager;
//	}
//	public void setManager(Lookup manager) {
//		this.manager = manager;
//	}
	public long getOrgKey() {
		return orgKey;
	}
	public void setOrgKey(long orgKey) {
		this.orgKey = orgKey;
	}
	public void setManagerKey(long managerKey) {
		this.managerKey = managerKey;
	}
	
	

}
