package org.cps.web.ui.model;

import org.cps.web.common.CPSConstants;

public class OrganizationVO implements Searchable{
	private long id;

	private String description;

	private String name;
	
	private int orgType;

	public int getOrgType() {
		return orgType;
	}

	public void setOrgType(int orgType) {
		this.orgType = orgType;
	}

	private long parentOrgId;

	private long passwordPolicyId;

	private Lookup parentOrg = new Lookup();//(HYSConstants.COMPONENT_ORGANIZATION);

	private Lookup passwordPolicy = new Lookup();//(HYSConstants.COMPONENT_ORGANIZATION);

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getParentOrgId() {
		return parentOrgId;
	}

	public void setParentOrgId(long parentOrgId) {
		this.parentOrgId = parentOrgId;
	}

	public long getPasswordPolicyId() {
		return passwordPolicyId;
	}

	public void setPasswordPolicyId(long passwordPolicyId) {
		this.passwordPolicyId = passwordPolicyId;
	}

	public Lookup getParentOrg() {
		return parentOrg;
	}

	public void setParentOrg(Lookup parentOrg) {
		this.parentOrg = parentOrg;
	}

	public Lookup getPasswordPolicy() {
		return passwordPolicy;
	}

	public void setPasswordPolicy(Lookup passwordPolicy) {
		this.passwordPolicy = passwordPolicy;
	}

	@Override
	public String getComponent() {
		return CPSConstants.COMPONENT_ORGANIZATION;
	}

	
}

