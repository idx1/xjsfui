package org.cps.web.ui.model;

public interface Searchable {

	public long getId();
	public String getName();
	public String getDescription();
	public String getComponent();
	public void setName(String selectedName);
	public void setId(long selectedId);
	
	
	
	
}
