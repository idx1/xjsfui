package org.cps.web.ui.beans;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.cps.web.client.ApplicationClientService;
import org.cps.web.client.IdentityClientService;
import org.idx.application.dob.AppTypeParamsDOB;
import org.idx.application.dob.AppTypeParamsDOB.APP_TYPE_PARAM_TYPE;
import org.idx.application.dob.ApplicationDOB;
import org.idx.application.dob.ApplicationTypeDOB;
import org.idx.application.dob.ConfigurationParamsDOB;
import org.idx.application.dob.ConfigurationParamsDOB.CONFIG_PARAM_TYPE;
import org.idx.application.dob.ConfiguratorDOB;
import org.idx.application.dob.MATCHING_ACTION;
import org.idx.application.dob.MATCHING_ACTION_CONDITION;
import org.idx.application.dob.MATCHING_CONDITION;
import org.idx.application.dob.MatchingActionDOB;
import org.idx.application.dob.MatchingRuleDOB;
import org.idx.application.dob.ProfileAttributeDOB;
import org.primefaces.event.FlowEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.github.jneat.jsf.SpringScopeView;

import lombok.Data;

@Component
@Data
@Scope("view")
public class ApplicationWizardBean implements Serializable {

	private ApplicationDOB application = new ApplicationDOB();

	private List<ApplicationTypeDOB> applicationTypes;

	private List<ProfileAttributeDOB> profileAttrs = new ArrayList<ProfileAttributeDOB>();

	private MatchingRuleDOB matchingRule = new MatchingRuleDOB();

	private List<MatchingActionDOB> matchingActions = new ArrayList<MatchingActionDOB>();

	private List<SelectItem> appTypes;

	private List<SelectItem> idxAttributes;

	private List<SelectItem> matchingCondition;

	private List<SelectItem> matchingActionCondition;

	private List<SelectItem> matchingActionList;

	private List<SelectItem> targetAttributes;

	private boolean skip;

	@Autowired
	ApplicationClientService applicationService;

	private ApplicationTypeDOB selectedApplicationType;

	public ApplicationWizardBean() {
		System.out.println("######### ApplicationWizardBean #########");

	}

	@PostConstruct
	public void init() {
		System.out.println("######### init #########");

		// BUG: This needs to be fixed

		applicationTypes = applicationService.getApplicationTypeList();

		appTypes = new ArrayList<SelectItem>();
		for (ApplicationTypeDOB applicationType : applicationTypes) {
			SelectItem selectItem = new SelectItem();
			selectItem.setLabel(applicationType.getDisplayName());
			selectItem.setValue(applicationType.getId());

			appTypes.add(selectItem);
		}

		matchingCondition = new ArrayList<SelectItem>();
		SelectItem matchingConditionEquals = new SelectItem();
		matchingConditionEquals.setLabel("Equals");
		matchingConditionEquals.setValue(MATCHING_CONDITION.EQUALS);

		SelectItem matchingConditionNotEquals = new SelectItem();
		matchingConditionNotEquals.setLabel("Not Equals");
		matchingConditionNotEquals.setValue(MATCHING_CONDITION.NOT_EQUALS);

		matchingCondition.add(matchingConditionEquals);
		matchingCondition.add(matchingConditionNotEquals);

		matchingActionCondition = new ArrayList<SelectItem>();

		SelectItem actConditionNoMatch = new SelectItem();
		actConditionNoMatch.setLabel("NO Match Found");
		actConditionNoMatch.setValue(MATCHING_ACTION_CONDITION.NO_MATCH);

		SelectItem actConditionSingleMatch = new SelectItem();
		actConditionSingleMatch.setLabel("Single Match Found");
		actConditionSingleMatch.setValue(MATCHING_ACTION_CONDITION.SINGLE_MATCH);

		SelectItem actConditionMultipleMatch = new SelectItem();
		actConditionMultipleMatch.setLabel("Multiple Match Found");
		actConditionMultipleMatch.setValue(MATCHING_ACTION_CONDITION.MULTIPLE_MATCH);

		matchingActionCondition.add(actConditionNoMatch);
		matchingActionCondition.add(actConditionSingleMatch);
		matchingActionCondition.add(actConditionMultipleMatch);

		matchingActionList = new ArrayList<SelectItem>();

		SelectItem createAct = new SelectItem();
		createAct.setLabel("Create New");
		createAct.setValue(MATCHING_ACTION.CREATE);

		SelectItem deleteAct = new SelectItem();
		deleteAct.setLabel("Delete");
		deleteAct.setValue(MATCHING_ACTION.DELETE);

		SelectItem updateAct = new SelectItem();
		updateAct.setLabel("Update");
		updateAct.setValue(MATCHING_ACTION.UPDATE);

		SelectItem ignoreAct = new SelectItem();
		ignoreAct.setLabel("Ignore");
		ignoreAct.setValue(MATCHING_ACTION.IGNORE);

		matchingActionList.add(createAct);
		matchingActionList.add(updateAct);
		matchingActionList.add(deleteAct);
		matchingActionList.add(ignoreAct);

	}

	public void save() {

		System.out.println("application : " + application);
		System.out.println("applicationTypes : " + applicationTypes);
		System.out.println("profileAttrs : " + profileAttrs);
		System.out.println("matchingRule : " + matchingRule);
		System.out.println("matchingActions : " + matchingActions);
		
		application.getProfile().setProfileAttr(profileAttrs);
		application.getProfile().setMatchingRule(matchingRule);
		application.getProfile().setMatchingActions(matchingActions);
		
		
		ConfiguratorDOB configurator = getConfigurator(selectedApplicationType);
		
		application.setConfigurator(configurator);
		
		ResponseEntity<ApplicationDOB> result = postRequest(application);
		
	      System.out.println("Status code:" + result.getStatusCode());
	      System.out.println("Body:" + result.getBody());

		
		FacesMessage msg = new FacesMessage("Successful", "Welcome :" + application.getName());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	
	//TODO: This all needs to go in utility class, most probable commons code
	// Need to change it with Generics implementation
	private static final String URL_CREATE_APPLICATION = "http://localhost:8100/aggregator/createApplication";

	private static final String AGGREGATOR_REST_URL = "http://localhost:8100/aggregator";
	private static final String AGGR_GET_APP_TYPE = "/getAppTypeList";
	private static final String AGGR_POST_CREATE_APP_TYPE = "/createApplicationType";
	private static final String AGGR_POST_CREATE_APP = "/createApplication";
		

	public ResponseEntity<ApplicationDOB> postRequest(ApplicationDOB application) {
		
		
		HttpHeaders headers = new HttpHeaders();
	      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	      headers.setContentType(MediaType.APPLICATION_JSON);
	 
	      RestTemplate restTemplate = new RestTemplate();
	 
	      
		// Data attached to the request.
	      HttpEntity<ApplicationDOB> requestBody = new HttpEntity<ApplicationDOB>(application , headers);
	 
	      URI reqURI = null;
	      ResponseEntity<ApplicationDOB>  result = null;
		try {
			reqURI = new URI(AGGREGATOR_REST_URL + AGGR_POST_CREATE_APP);
		    result = restTemplate.postForEntity(reqURI, requestBody, ApplicationDOB.class);

		} catch (Exception e1) {
		      System.out.println("Body:" + e1.getMessage());
		      System.out.println("Body:" + e1.getCause());

			e1.printStackTrace();
		}
 	      
	      if (result.getStatusCode() == HttpStatus.OK) {
	    	  	ApplicationDOB e = result.getBody();
	    	  	System.out.println("(Client Side) Application Type Created: "+ e.getId());
	      }


		return result;
		
	}
	
	private ConfiguratorDOB getConfigurator(ApplicationTypeDOB applicationType) {

		ConfiguratorDOB configurator = new ConfiguratorDOB();
		
		configurator.setName(selectedApplicationType.getName());
		configurator.setDisplayName(selectedApplicationType.getDisplayName());
		
		String className = null;
		configurator.setClassName(className);

		List<ConfigurationParamsDOB> configParams = new ArrayList<ConfigurationParamsDOB>();
		
		List<AppTypeParamsDOB> appParams = applicationType.getAppTypeParams();
		
		for(AppTypeParamsDOB appParam: appParams) {
			
			ConfigurationParamsDOB configParam = new ConfigurationParamsDOB();
			
			configParam.setName(appParam.getName());
			configParam.setDescription(appParam.getDescription());
			configParam.setValue(appParam.getValue());
			
			APP_TYPE_PARAM_TYPE paramType = appParam.getType();
			
			CONFIG_PARAM_TYPE configParamtype = null;
			
			if(paramType.equals(APP_TYPE_PARAM_TYPE.STRING))
				configParamtype = CONFIG_PARAM_TYPE.STRING;
			else if(paramType.equals(APP_TYPE_PARAM_TYPE.SECRET))
				configParamtype = CONFIG_PARAM_TYPE.SECRET;
			else if(paramType.equals(APP_TYPE_PARAM_TYPE.DATE))
				configParamtype = CONFIG_PARAM_TYPE.DATE;
			else if(paramType.equals(APP_TYPE_PARAM_TYPE.BOOLEAN))
				configParamtype = CONFIG_PARAM_TYPE.BOOLEAN;
			else if(paramType.equals(APP_TYPE_PARAM_TYPE.NUMBER))
				configParamtype = CONFIG_PARAM_TYPE.NUMBER;

			configParam.setType(configParamtype);
			
			configParams.add(configParam);
		}
		
		
		configurator.setConfigParams(configParams);
		
		
		
		String type = null;
		configurator.setType(type);
		
		return configurator;
	}

	public boolean isSkip() {
		return skip;
	}

	public void setSkip(boolean skip) {
		this.skip = skip;
	}

	public String onFlowProcess(FlowEvent event) {

		System.out.println("onFlowProcess called : " + event.getOldStep());
		System.out.println("onFlowProcess called : " + event.getNewStep());
		System.out.println("onFlowProcess called : " + event.getSource());

		if (event.getOldStep().equals("basicInfo")) {
			System.out.println("configurator type : " + application.getConfiguratorType());

			System.out.println("applicationTypes : " + applicationTypes);
			System.out.println("selectedApplicationType : " + selectedApplicationType);

//			if (selectedApplicationType == null) {

				for (ApplicationTypeDOB applicationType : applicationTypes) {
					if (application.getConfiguratorType() == applicationType.getId())
						selectedApplicationType = applicationType;

					System.out.println("selectedApplicationType : " + selectedApplicationType);

				}
				
				
				System.out.println("ABCD : " + selectedApplicationType.getAppTypeParams());
//			}

		}

		if (event.getOldStep().equals("profile") && event.getNewStep().equals("rule")) {

			if (idxAttributes == null)
				idxAttributes = new ArrayList<SelectItem>();

			if (targetAttributes == null)
				targetAttributes = new ArrayList<SelectItem>();

			System.out.println("populating select from profileAttrs : " + profileAttrs);

			for (ProfileAttributeDOB profileAttribute : profileAttrs) {

				boolean idxAttrExists = false;
				boolean targetAttrExists = false;

				String idxName = profileAttribute.getName();
				String targetName = profileAttribute.getTargetAttrName();

				for (SelectItem idxAttribute : idxAttributes) {
					String value = (String) idxAttribute.getValue();

					if (value.equals(idxName))
						idxAttrExists = true;
				}

				for (SelectItem targetAttribute : targetAttributes) {
					String value = (String) targetAttribute.getValue();

					if (value.equals(targetName))
						targetAttrExists = true;
				}

				if (!idxAttrExists) {
					SelectItem idxAttr = new SelectItem();
					idxAttr.setLabel(idxName);
					idxAttr.setValue(idxName);

					idxAttributes.add(idxAttr);
				}

				if (!targetAttrExists) {
					SelectItem tgtAttr = new SelectItem();
					tgtAttr.setLabel(targetName);
					tgtAttr.setValue(targetName);

					targetAttributes.add(tgtAttr);
				}

			}

		}

		if (skip) {
			skip = false; // reset in case user goes back
			return "confirm";
		} else {
			return event.getNewStep();
		}
	}

	public void addProfileAttributeRow() {
		System.out.println("add profileAttrs : " + profileAttrs);
		ProfileAttributeDOB attr = new ProfileAttributeDOB();
		profileAttrs.add(attr);
	}

	public void updateValue() {
	}

	public void updateProfileAttributeIDXName(ProfileAttributeDOB profileAttribute) {
		System.out.println("updateProfileAttributeIDXName : " + profileAttribute);

		// if(idxAttributes == null)
		// idxAttributes = new ArrayList<SelectItem>();
		//
		// SelectItem selectItem = new SelectItem();
		// selectItem.setLabel(profileAttribute.getName());
		// selectItem.setValue(profileAttribute.getName());
		//
		//
		// idxAttributes.add(selectItem);

	}

	public void updateProfileAttributeTargetName(ProfileAttributeDOB profileAttribute) {
		System.out.println("updateProfileAttributeTargetName : " + profileAttribute);

		// if(targetAttributes == null)
		// targetAttributes = new ArrayList<SelectItem>();
		//
		//
		// if(targetAttributes == null)
		// targetAttributes = new ArrayList<SelectItem>();
		//
		// SelectItem selectItem = new SelectItem();
		// selectItem.setLabel(profileAttribute.getTargetAttrName());
		// selectItem.setValue(profileAttribute.getTargetAttrName());
		//
		//
		// targetAttributes.add(selectItem);

	}

	public void removeProfileAttributeRow(ProfileAttributeDOB profileAttribute) {

		System.out.println("OOOO profileAttribute : " + profileAttribute);
		System.out.println("profileAttrs : " + profileAttrs);

		ProfileAttributeDOB attr = new ProfileAttributeDOB();

		profileAttrs.remove(profileAttribute);

	}

	public void removeMatchingActionRow(MatchingActionDOB action) {
		matchingActions.remove(action);
	}

	public void addMatchingActionRow() {

		MatchingActionDOB action = new MatchingActionDOB();

		matchingActions.add(action);
	}
}
