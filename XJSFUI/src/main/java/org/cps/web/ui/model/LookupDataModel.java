package org.cps.web.ui.model;


import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

public class LookupDataModel extends ListDataModel<Lookup> implements Serializable, SelectableDataModel<Lookup>{

	public LookupDataModel() {
	}

	public LookupDataModel(List<Lookup> data) {
		super(data);
	}

	@Override
	public Lookup getRowData(String rowKey) {
		List<Lookup> lookups = (List<Lookup>) getWrappedData();
        System.out.println("lookups : "+ lookups);
        System.out.println("getRowData : "+ rowKey);
        for(Lookup lookup : lookups) {
            if((lookup.getId()).equals(rowKey))
                return lookup;
        }

        return null;
	}

	@Override
	public Object getRowKey(Lookup lookup) {
        System.out.println("lookup.getId : "+ lookup.getId());
		return lookup.getId();
	}

}
