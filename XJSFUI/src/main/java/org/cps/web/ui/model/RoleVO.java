package org.cps.web.ui.model;

import org.cps.web.common.CPSConstants;

public class RoleVO implements Searchable{
	private long id;

	private String description;

	private String name;
	
	private long parentRole;

	private long roleAdmin;

	private long parentOrgId;

	private long passwordPolicyId;

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getParentOrgId() {
		return parentOrgId;
	}

	public void setParentOrgId(long parentOrgId) {
		this.parentOrgId = parentOrgId;
	}

	public long getPasswordPolicyId() {
		return passwordPolicyId;
	}

	public void setPasswordPolicyId(long passwordPolicyId) {
		this.passwordPolicyId = passwordPolicyId;
	}

 
 
	public long getParentRole() {
		return parentRole;
	}

	public void setParentRole(long parentRole) {
		this.parentRole = parentRole;
	}

	public long getRoleAdmin() {
		return roleAdmin;
	}

	public void setRoleAdmin(long roleAdmin) {
		this.roleAdmin = roleAdmin;
	}

	@Override
	public String getComponent() {
		return CPSConstants.COMPONENT_ROLES;
	}

	
}

