package org.cps.web.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.management.relation.Role;

import org.cps.web.common.CPSConstants;
import org.cps.web.ui.model.SearchResultData;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@ManagedBean(name="searchService")
@SessionScoped
@Component
public class ComponentSearchService implements Serializable{
	
	IdentityClientService identityService;
//	OrganizationClientService orgService;
//	RoleClientService roleService;
//	SchedulerClientService schedulerService;
//	ApplicationClientService applicationService;
//	ApplicationInstanceClientService applicationInstanceService;
	
	public IdentityClientService getIdentityService() {
		return identityService;
	}

	public void setIdentityService(IdentityClientService identityService) {
		this.identityService = identityService;
	}

//	public OrganizationClientService getOrgService() {
//		return orgService;
//	}
//
//	public void setOrgService(OrganizationClientService orgService) {
//		this.orgService = orgService;
//	}

//	public RoleClientService getRoleService() {
//		return roleService;
//	}
//
//	public void setRoleService(RoleClientService roleService) {
//		this.roleService = roleService;
//	}
//
//	public SchedulerClientService getSchedulerService() {
//		return schedulerService;
//	}
//
//	public void setSchedulerService(SchedulerClientService schedulerService) {
//		this.schedulerService = schedulerService;
//	}
//
//	public ApplicationClientService getApplicationService() {
//		return applicationService;
//	}
//
//	public void setApplicationService(ApplicationClientService applicationService) {
//		this.applicationService = applicationService;
//	}
//
//	public ApplicationInstanceClientService getApplicationInstanceService() {
//		return applicationInstanceService;
//	}
//
//	public void setApplicationInstanceService(
//			ApplicationInstanceClientService applicationInstanceService) {
//		this.applicationInstanceService = applicationInstanceService;
//	}

	public List<SearchResultData> search(String component, String value) {
		List<SearchResultData> searchResult = new ArrayList<SearchResultData>();
		
		System.out.println(component + " ##### "+ value);
		
		if(component.equals(CPSConstants.COMPONENT_USER)){
			searchResult = identityService.search(value);
		}
		else if(component.equals(CPSConstants.COMPONENT_ORGANIZATION)){
			searchResult = identityService.search(value);
		}
//		else if(component.equals(CPSConstants.COMPONENT_ROLES)){
//			searchResult = roleService.search(value);
//		}
//		else if(component.equals(CPSConstants.COMPONENT_APPLICATION)){
//			searchResult = applicationService.search(value);
//		}
//		else if(component.equals(CPSConstants.COMPONENT_APPLICATION_INSTANCE)){
//			searchResult = applicationInstanceService.search(value);
//		}
//		else if(component.equals(CPSConstants.COMPONENT_SCHEDULER)){
//			searchResult = schedulerService.search(value);
//		}
		
		return searchResult;
	}
	
	private void populateRandomComponent(String searchComponent, List<SearchResultData> list) {
		
		for (int i = 0; i < 40; i++)
			list.add(new SearchResultData(i, searchComponent + "_HYDS_" + i, searchComponent + " Farzi Description",
					searchComponent));
	}


}
