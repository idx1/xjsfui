package org.cps.web.client;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.List;


import org.apache.tomcat.util.codec.binary.Base64;
import org.cps.identity.exception.UserManagerException;
import org.cps.identity.model.User;
import org.cps.identity.service.RoleManagerService;
import org.cps.identity.service.UserManagerService;
import org.cps.server.utils.DataMappingUtils;
import org.cps.web.exception.OrganizationCreationException;
import org.cps.web.exception.RoleCreationException;
import org.cps.web.exception.UserCreationException;
import org.cps.web.ui.beans.UserSearchCriteria;
import org.cps.web.ui.model.OrganizationVO;
import org.cps.web.ui.model.RoleVO;
import org.cps.web.ui.model.SearchResultData;
import org.cps.web.ui.model.UserVO;
import org.cps.web.utils.WebConstants;
import org.idx.identity.dob.UserAttributeDOB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Scope("session")
public class IdentityClientService {

	private static final String REST_SERVICE_URI = "http://localhost:8080/identity";
	
	//NOTE: THIS NEEDS TO BE REMOVED with REST API CALL
	@Autowired
	UserManagerService userService;

	@Autowired
	RoleManagerService roleService;


	public User findUserByUserName(String userName) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String createRole(RoleVO roleVO) throws RoleCreationException {
		return null;
	}
	
	public String createOrg(OrganizationVO orgVO) throws OrganizationCreationException {
		return null;
	}
	
	public String createUser(UserVO userVO) throws UserManagerException {
		
		System.out.println("create User : " + userVO);

		UsernamePasswordAuthenticationToken token = null;
		
		UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        
        
		System.out.println("authentication : " + authentication);
		System.out.println("authentication : " + authentication.getName());
		System.out.println("authentication : " + authentication.getAuthorities());
		System.out.println("authentication : " + authentication.getCredentials());
		System.out.println("authentication : " + authentication.getDetails());
		System.out.println("authentication : " + authentication.getPrincipal());
		
		
		HttpHeaders headers = createHeaders((String)authentication.getPrincipal(), "admin");

		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<UserVO> respEntity = restTemplate.exchange(REST_SERVICE_URI+"/createUser/", HttpMethod.POST, entity, 
        		UserVO.class );
        
        
		System.out.println("respEntity : " + respEntity);

		//NOTE: THIS NEEDS TO BE REMOVED with REST API CALL		
		User user = DataMappingUtils.convertUserVO(userVO);

		User savedUser = userService.createUser(user);
		
		
		System.out.println("respEntity : " + WebConstants.USER_CREATION_SUCCESSFUL);

		
		if(true)
			return WebConstants.USER_CREATION_SUCCESSFUL;
		else 
			throw new UserManagerException("GOTE SHORT");		
		
		

		/*
		
        System.out.println("Testing create User API----------");
        
        RestTemplateBuilder tb = new RestTemplateBuilder();
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        
		System.out.println("authentication : " + authentication);
		System.out.println("authentication : " + authentication.getName());
		System.out.println("authentication : " + authentication.getAuthorities());
		System.out.println("authentication : " + authentication.getCredentials());
		System.out.println("authentication : " + authentication.getDetails());
		System.out.println("authentication : " + authentication.getPrincipal());

        
        RestTemplate restTemplate = new RestTemplate();
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI+"/createUser/", user, UserVO.class);
        System.out.println("Location : "+uri.toASCIIString());
        
        */

	}
	
	
	HttpHeaders createHeaders(String username, String password) {
	    HttpHeaders acceptHeaders = new HttpHeaders() {
	        {
	            set(HttpHeaders.ACCEPT, 
	                MediaType.APPLICATION_JSON.toString());
	        }
	    };
	    String authorization = username + ":" + password;
	    String basic = new String(Base64.encodeBase64
	        (authorization.getBytes(Charset.forName("US-ASCII"))));
	    
 	    
	    acceptHeaders.set("Authorization", "Basic " + basic);
	 
	    return acceptHeaders;
	}

	public List<UserVO> searchUser(UserSearchCriteria searchCriteria) throws UserManagerException {

		List<User> users = userService.findUser(searchCriteria);
				
	List<UserVO> userVOs = DataMappingUtils.convertListToUserVO(users);
		
		return userVOs;
	}

	public List<SearchResultData> search(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<UserAttributeDOB> getUserAttributes() {
		List<UserAttributeDOB> attrs = null;
		try {
			attrs = userService.getUserAttributes();
		} catch (UserManagerException e) {
			e.printStackTrace();
		}
		return attrs;
	}


}
