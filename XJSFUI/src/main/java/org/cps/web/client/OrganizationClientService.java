package org.cps.web.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.core.MediaType;

import org.cps.framework.logging.CPSLogger;
import org.cps.web.ui.model.OrganizationVO;
import org.cps.web.ui.model.SearchResultData;
import org.cps.web.utils.JavaToJsonConverter;
import org.cps.web.utils.JsonToJavaConverter;
import org.cps.web.utils.WebUtils;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

@ManagedBean(name="orgService")
@SessionScoped
public class OrganizationClientService implements Serializable{
	private static String REST_SERVICE="orgService"; 
	
	CPSLogger logger = new CPSLogger("OrganizationClientService");

	public String create(OrganizationVO org){
		String status = null;
		try {
			WebResource resource = WebUtils.getWebResource(REST_SERVICE);
			String orgJson = JavaToJsonConverter.objectToJson(org);
			logger.debug("orgJson : "+ orgJson);
			resource = resource.path("create");
			ClientResponse response = resource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, orgJson);
			System.out.println("resource : " + resource.getURI());
			status = (String) JsonToJavaConverter.jsonToObject(response.getEntity(String.class));	
			logger.debug("status : "+ status);
		} catch (UniformInterfaceException e) {
			e.printStackTrace();
		} catch (ClientHandlerException e) {
			e.printStackTrace();
		}
		return status;
	};



	public List<SearchResultData> search(String searchString) {

	List<SearchResultData> searchData = null;
	try {
		WebResource resource = WebUtils.getWebResource(REST_SERVICE);
		String searchStringJson = JavaToJsonConverter.objectToJson(searchString);
		logger.debug("searchStringJson : "+ searchStringJson);
		resource = resource.path("search");
		ClientResponse response = resource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, searchStringJson);
		System.out.println("resource : " + resource.getURI());
		List<OrganizationVO> orgSearchData = (List<OrganizationVO>) JsonToJavaConverter.jsonToObject(response.getEntity(String.class));
		
		searchData = getDataInSearchableFormat(orgSearchData);
		
		logger.debug("status : "+ searchData);
	} catch (UniformInterfaceException e) {
		e.printStackTrace();
	} catch (ClientHandlerException e) {
		e.printStackTrace();
	}
	return searchData;
}

private List<SearchResultData> getDataInSearchableFormat(
		List<OrganizationVO> orgSearchData) {
	
	List<SearchResultData> searchData = new ArrayList<SearchResultData>();

	for(OrganizationVO org:orgSearchData){
		SearchResultData data = new SearchResultData(org.getId(), 
														org.getName(), 
														org.getDescription(), 
														org.getComponent());
		searchData.add(data);
	}
	return searchData;
}





}
