package org.cps.web.client;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.idx.application.dob.EntitlementDOB;
import org.idx.application.dob.ApplicationDOB;
import org.idx.application.dob.ApplicationProfileDOB;
import org.idx.application.dob.ApplicationTypeDOB;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class ApplicationClientService {

	private static final String AGGREGATOR_REST_URL = "http://localhost:8100/aggregator";
	private static final String AGGR_GET_APP_TYPE = "/getAppTypeList";

	private static final String APP_REST_URL = "http://localhost:8100/application";
	private static final String APP_GET_APP_LIST = "/getApplicationList";
	private static final String APP_GET_APP_ENT_LIST = "/getEntitlementsForApplication";
	private static final String APP_CREATE_APP_PROFILE = "/createApplicationProfile";
	private static final String APP_GET_APP_BY_NAME = "/findApplicationByName";
	
	

	public List<ApplicationTypeDOB> getApplicationTypeList() {
		
		HttpHeaders headers = new HttpHeaders();
	      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	      headers.setContentType(MediaType.APPLICATION_JSON);
	 
	      RestTemplate restTemplate = new RestTemplate();
	 
	      // Data attached to the request.
	      HttpEntity<ApplicationTypeDOB[]> requestBody = new HttpEntity<ApplicationTypeDOB[]>(headers);
	 

	      URI reqURI = null;
	      ApplicationTypeDOB[] result = null;
	      
		try {
			reqURI = new URI(AGGREGATOR_REST_URL + AGGR_GET_APP_TYPE);

		    
		    
		    ResponseEntity<ApplicationTypeDOB[]> response = restTemplate.exchange(reqURI, HttpMethod.GET, requestBody, ApplicationTypeDOB[].class);

		    result = response.getBody();
		    
		    for(ApplicationTypeDOB app1: result) {
		    	System.out.println(app1.getDisplayName());
		    }
	    
		      System.out.println("Status code:" + response.getStatusCode());
		      System.out.println("Body:" + result);

		} catch (Exception e1) {
		      System.out.println("Body:" + e1.getMessage());
		      System.out.println("Body:" + e1.getCause());

			e1.printStackTrace();
		}
		
		List<ApplicationTypeDOB> appList= new ArrayList<ApplicationTypeDOB>();
		
		for(ApplicationTypeDOB appTypeDOB:result) {
			
			appList.add(appTypeDOB);
			
		}


		return appList;
	}


	public ApplicationDOB[] getApplicationList() {
		HttpHeaders headers = new HttpHeaders();
	      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	      headers.setContentType(MediaType.APPLICATION_JSON);
	 
	      RestTemplate restTemplate = new RestTemplate();
	 
	      // Data attached to the request.
	      HttpEntity<ApplicationDOB[]> requestBody = new HttpEntity<ApplicationDOB[]>(headers);
	 

	      URI reqURI = null;
	      ApplicationDOB[] result = null;
			try {
				reqURI = new URI(APP_REST_URL + APP_GET_APP_LIST);
//			    result = restTemplate.postForEntity(reqURI, requestBody, ApplicationDOB.class);
			    
			    
			    ResponseEntity<ApplicationDOB[]> response = restTemplate.exchange(reqURI, HttpMethod.GET, requestBody, ApplicationDOB[].class);

			    result = response.getBody();
			    
 
			} catch (Exception e1) {
			      System.out.println("Body:" + e1.getMessage());
			      System.out.println("Body:" + e1.getCause());

				e1.printStackTrace();
			}

			return result;

 	}


	public EntitlementDOB[] getEntitlementsForApplication(String name) {
		HttpHeaders headers = new HttpHeaders();
	      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	      headers.setContentType(MediaType.APPLICATION_JSON);
	 
	      RestTemplate restTemplate = new RestTemplate();
	 
	      // Data attached to the request.
	      HttpEntity<EntitlementDOB[]> requestBody = new HttpEntity<EntitlementDOB[]>(headers);
	      URI reqURI = null;
	      EntitlementDOB[] result = null;
			try {
				reqURI = new URI(APP_REST_URL + APP_GET_APP_ENT_LIST);
				
				UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(APP_REST_URL + APP_GET_APP_ENT_LIST)
				        .queryParam("name", name);

				
//			    result = restTemplate.postForEntity(reqURI, requestBody, ApplicationDOB.class);
			    
			    
			    ResponseEntity<EntitlementDOB[]> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, requestBody, EntitlementDOB[].class);

			    result = response.getBody();
			    

			} catch (Exception e1) {
			      System.out.println("Body:" + e1.getMessage());
			      System.out.println("Body:" + e1.getCause());

				e1.printStackTrace();
			}

			return result;
	}


	public ResponseEntity<ApplicationProfileDOB> createApplicationProfile(ApplicationProfileDOB profile) throws Exception {
		HttpHeaders headers = new HttpHeaders();
	      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	      headers.setContentType(MediaType.APPLICATION_JSON);
	 
	      RestTemplate restTemplate = new RestTemplate();
	 
	      
		// Data attached to the request.
	      HttpEntity<ApplicationProfileDOB> requestBody = new HttpEntity<ApplicationProfileDOB>(profile , headers);
	 
	      URI reqURI = null;
	      ResponseEntity<ApplicationProfileDOB>  result = null;
		 
			reqURI = new URI(APP_REST_URL + APP_CREATE_APP_PROFILE);
		    result = restTemplate.postForEntity(reqURI, requestBody, ApplicationProfileDOB.class);
		      System.out.println("result:" + result);

		 
	      
	      if (result.getStatusCode() == HttpStatus.OK) {
	    	  ApplicationProfileDOB e = result.getBody();
	    	  	System.out.println("(Client Side) Application Type Created: "+ e.getId());
	      }


		return result;
		
		
	}


	public ApplicationDOB getApplication(String application) {
		HttpHeaders headers = new HttpHeaders();
	      headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	      headers.setContentType(MediaType.APPLICATION_JSON);
	 
	      RestTemplate restTemplate = new RestTemplate();
	 
	      // Data attached to the request.
	      HttpEntity<ApplicationDOB> requestBody = new HttpEntity<ApplicationDOB>(headers);
	 

	      URI reqURI = null;
	      ApplicationDOB result = null;
			try {
//				reqURI = new URI(APP_REST_URL + APP_GET_APP_BY_NAME);
//			    result = restTemplate.postForEntity(reqURI, requestBody, ApplicationDOB.class);
			    
				UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(APP_REST_URL + APP_GET_APP_BY_NAME)
				        .queryParam("name", application);

			    ResponseEntity<ApplicationDOB> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, requestBody, ApplicationDOB.class);

			    result = response.getBody();
			    

			} catch (Exception e1) {
			      System.out.println("Body:" + e1.getMessage());
			      System.out.println("Body:" + e1.getCause());

				e1.printStackTrace();
			}

			return result;

	}

}
