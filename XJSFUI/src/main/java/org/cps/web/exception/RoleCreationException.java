package org.cps.web.exception;

public class RoleCreationException extends Exception {

	public RoleCreationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RoleCreationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public RoleCreationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RoleCreationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RoleCreationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
