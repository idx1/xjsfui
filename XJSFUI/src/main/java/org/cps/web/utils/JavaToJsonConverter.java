package org.cps.web.utils;

import java.util.List;

import flexjson.JSONSerializer;


public class JavaToJsonConverter {
	
	public static <T> String listToJson(List<T> list){
		String json = new JSONSerializer().deepSerialize(list);
		return json;
	}
	
	public static <T> String objectToJson(Object obj){
		String json = new JSONSerializer().deepSerialize(obj);
		return json;
	}
}