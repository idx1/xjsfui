package org.cps.web.utils;


import java.util.List;
import java.util.Map;

import flexjson.JSONDeserializer;

public class JsonToJavaConverter {

	public static <T> List<T> jsonToList(String json){
		List<T> lists = new JSONDeserializer<List<T>>().deserialize(json);
		return lists;
	}
	
	public static <T> Object jsonToObject(String json) {
//		Logger.debug("json : "+ json);

		Object obj = new JSONDeserializer<T>().deserialize(json);
		return obj;
	}
	
	public static Map<String, String> jsonToMap(String json) {
		Map<String, String> map = new JSONDeserializer<Map<String, String>>().deserialize(json);
		return map;
	}
}