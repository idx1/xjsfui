package org.cps.web.utils;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;


public class WebUtils {

	
	public static Object getBeanInstance(String beanName, Class beanClass) {
		FacesContext fctx = FacesContext.getCurrentInstance();
		Application application = fctx.getApplication();
		ExpressionFactory expressionFactory = application.getExpressionFactory();
		ELContext context = fctx.getELContext();
		ValueExpression createValueExpression = expressionFactory.createValueExpression(context, beanName, beanClass);		
		
		Object service = createValueExpression.getValue(context);
		
		return service;

	}

	/**
	 * HARD CODING needs to be removed
	 */
	private static String WEB_PATH = "http://localhost:8080/CPS/rest/";
	private static ClientConfig config;	

	public static WebResource getWebResource(String path) {
		WebResource webResource = getClient().resource(WEB_PATH + path + "/");
		return webResource;
	}
	
	public static Client getClient() {
		Client client = Client.create(getConfig());
		return client;
	}

	public static ClientConfig getConfig() {
		if(null == config){
			config = new DefaultClientConfig();
			config.getProperties().put(ClientConfig.PROPERTY_FOLLOW_REDIRECTS, true);
		}
		return config;
	}

}
