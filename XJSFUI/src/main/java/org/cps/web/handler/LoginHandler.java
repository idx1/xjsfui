package org.cps.web.handler;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
 


import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import org.cps.web.auth.CustomAuthenticationManager;
  

@ManagedBean(name="loginHandler")
@RequestScoped
public class LoginHandler {

	 
	private String userName;
	 
	private String password;
	
	public LoginHandler(){
		
	}
	
    @ManagedProperty(value = "#{CustomAuthenticationManager}")
	CustomAuthenticationManager customAuthenticationManager;

	public CustomAuthenticationManager getCustomAuthenticationManager() {
		return customAuthenticationManager;
	}

	public void setCustomAuthenticationManager(
			CustomAuthenticationManager customAuthenticationManager) {
		this.customAuthenticationManager = customAuthenticationManager;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}



	public String login() {		
//		try {
//			customAuthenticationManager.authenticate(userName, password);
//		} catch (AuthenticationException e) {
//			e.printStackTrace();
//	        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage());
//	        FacesContext.getCurrentInstance().addMessage(null, message);  
//			return null;
//		}
//        System.out.println("toWelcomePage : ");

		return "toWelcomePage";
	}

	public void logout() {		
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        HttpServletRequest request = (HttpServletRequest)ectx.getRequest();

         HttpSession session = (HttpSession)ectx.getSession(true);
         
         
         
         String url = request.getContextPath()+"/j_spring_security_logout";
         
         System.out.println("Logout URL : "+ url);
         
         try {
        	session.invalidate();
			ectx.redirect(url);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	
	
	public String forgotPassword() {
		System.out.println("toForgotPasswordPage");

		return "toForgotPasswordPage";
	}

	public String selfRegistration() {
		System.out.println("selfRegistration");

		return "toSelfRegistrationdPage";
	}

	public String loginPage() {
		System.out.println("toLoginPage");

		return "toLoginPage";
	}

 }
