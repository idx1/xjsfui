package org.cps.sync.dao;

import java.util.List;

import org.cps.sync.model.EVENT_STATE;
import org.cps.sync.model.SyncEvent;
import org.cps.sync.model.SyncProfile;

public interface SyncEngineDAO {
	
	public void createSyncProfile(SyncProfile profile) ;

	public SyncProfile getSyncProfile(String profileName) ;

	public void createSyncEvent(SyncEvent event) ;

	public List<SyncEvent> getOpenEventsForProfile(String profileName);

	public void updateEventStatus(SyncEvent event, EVENT_STATE eventState);

}
