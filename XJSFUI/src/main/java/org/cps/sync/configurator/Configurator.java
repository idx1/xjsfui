package org.cps.sync.configurator;

import java.util.List;

import org.cps.framework.exception.CPSException;
import org.cps.sync.model.SyncBase;
import org.cps.sync.model.SyncEvent;

 
public interface Configurator {

	public List<SyncEvent> performReconciliation(SyncBase base) throws CPSException;

}
