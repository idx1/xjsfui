package org.cps.sync.service;

import java.util.List;

import org.cps.sync.model.EVENT_STATE;
import org.cps.sync.model.SyncEvent;
import org.cps.sync.model.SyncProfile;

 
public interface SyncEngineService {

	public void createSyncProfile(SyncProfile profile);

	public SyncProfile getSyncProfile(String profileName);

	public void createEvents(List<SyncEvent> events);

	public List<SyncEvent> getOpenEventsForProfile(String profileName);

	public void updateEventStatus(SyncEvent event, EVENT_STATE eventState);

}
