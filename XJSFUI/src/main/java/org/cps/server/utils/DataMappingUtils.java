package org.cps.server.utils;

import java.util.ArrayList;
import java.util.List;

import org.cps.identity.model.User;
import org.cps.web.ui.model.UserVO;

public class DataMappingUtils {

	public static User convertUserVO(UserVO userVO) {

		User user = new User();
		
		user.setFirstName(userVO.getFirstName());
		user.setMiddleName(userVO.getMiddleName());
		user.setLastName(userVO.getLastName());
		user.setEmail(userVO.getEmail());
		user.setUserName(userVO.getUserName());
		user.setPassword(userVO.getPassword());
		
		
		return user;
	}

	public static List<UserVO> convertListToUserVO(List<User> users) {

		List<UserVO> userVOs = new ArrayList<UserVO>();
		
		for(User user: users) {
			UserVO userVO = new UserVO();
			
			userVO.setUserKey(user.getId());
			userVO.setUserName(user.getUserName());
			userVO.setFirstName(user.getFirstName());
			userVO.setMiddleName(user.getMiddleName());
			userVO.setLastName(user.getLastName());
			userVO.setEmail(user.getEmail());
			
			userVOs.add(userVO);
		}
		
		return userVOs;
	}

}
