package org.cps.server.rest.test;

import java.net.URI;

import org.cps.identity.model.User;
import org.cps.web.ui.model.UserVO;
import org.springframework.web.client.RestTemplate;

public class RestAPITest {
	
	private static final String REST_SERVICE_URI = "http://localhost:8080/api";
	
	public static void main(String args[]) {	
		createUser();
	}
    private static void createUser() {
        System.out.println("Testing create User API----------");
        RestTemplate restTemplate = new RestTemplate();
        UserVO user = new UserVO();
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI+"/user/", user, UserVO.class);
        System.out.println("Location : "+uri.toASCIIString());
    }
  

}
