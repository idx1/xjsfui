package org.cps.server.rest;

import java.net.URI;

import org.cps.identity.exception.UserManagerException;
import org.cps.identity.model.User;
import org.cps.identity.service.UserManagerService;
import org.cps.server.utils.DataMappingUtils;
import org.cps.web.exception.UserCreationException;
import org.cps.web.ui.model.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api")
public class UserController {
	
	@Autowired
	UserManagerService userService;
	
	@PostMapping("/createUser")
	public ResponseEntity<Object> createUser(@RequestBody UserVO userVO) {
		
		System.out.println("################ Web Services call for createUser ################");
		
		
		User user = DataMappingUtils.convertUserVO(userVO);
		
		User savedUser = null;
		try {
			savedUser = userService.createUser(user);
		} catch (UserManagerException e) {
			e.printStackTrace();
		}
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedUser.getId()).toUri();

		return ResponseEntity.created(location).build();		
	}


}
