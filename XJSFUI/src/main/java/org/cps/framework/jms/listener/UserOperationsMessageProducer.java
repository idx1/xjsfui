package org.cps.framework.jms.listener;

import org.cps.framework.jms.message.UserMessage;
import org.cps.identity.common.UserManagerConstants;
import org.cps.identity.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
 public class UserOperationsMessageProducer {
	
	  @Autowired 
	  private JmsTemplate jmsTemplate;
	  
	  @Async
	  public void postUserCreationMessage(User user) {
		  
		  UserMessage userMessage = new UserMessage(UserManagerConstants.USER_CREATE , user);
		  
		  System.out.println("Sending message");
		  
		  jmsTemplate.convertAndSend("UserOperationMessageQueue",userMessage);
		  
	  }

}
