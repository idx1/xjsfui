package org.cps.framework.jms.listener;

import org.cps.framework.jms.message.UserMessage;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class UserOperationsListener {
	
	  @JmsListener(destination = "UserOperationMessageQueue", containerFactory = "CPSFactory")
	  public void receiveMessage(UserMessage userMessage) {

		    System.out.println("Received <" + userMessage + ">");
		    System.out.println("Received <" + userMessage.getOperation() + ">");
		    System.out.println("Received <" + userMessage.getUser() + ">");


	  }


}
