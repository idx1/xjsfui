package org.cps.framework.jms.message;

import java.io.Serializable;

import org.cps.identity.model.User;

public class UserMessage implements Serializable{
	
	public UserMessage(){
		System.out.println("User Message Ban raha hai bhaiya");
	}
	
	private String operation;
	
	private User user;

	public UserMessage(String operation, User user) {
		this.operation = operation;
		this.user=user;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	

}
