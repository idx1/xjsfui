package org.cps.framework.exception;

public class CPSAuthenticationException extends CPSException {

	public CPSAuthenticationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CPSAuthenticationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CPSAuthenticationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CPSAuthenticationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CPSAuthenticationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
