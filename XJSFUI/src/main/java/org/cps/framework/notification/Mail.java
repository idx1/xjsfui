package org.cps.framework.notification;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class Mail {

    private String from;
    private String to;
    private String subject;
    private String content;
    Map model = new HashMap();

}
