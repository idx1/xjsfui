package org.cps.framework.security;

import java.util.Collection;

import org.cps.framework.exception.CPSAuthenticationException;
import org.cps.identity.dao.UserManagerDAO;
import org.cps.identity.exception.UserManagerException;
import org.cps.identity.service.UserManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	
	@Autowired
	private UserManagerService userManagerService;


	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {

		System.out.println("#### Hello AUTH ####" + auth);
		System.out.println("#### name #### : " + auth.getName());
		System.out.println("#### password #### : " + auth.getCredentials());
				
		String userName = auth.getName().trim();
		String password = null; 
		
		if(auth.getCredentials() != null)
			password = auth.getCredentials().toString(); 
		
		try {
			userManagerService.login(userName, password);
		} catch (UserManagerException e) {
			e.printStackTrace();
	        throw new BadCredentialsException("Authentication failed for user : " + userName + " , Reason : " + e.getMessage()); 
		}
		
         return new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials() ); 
 	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	    //return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);	
	    }

}
