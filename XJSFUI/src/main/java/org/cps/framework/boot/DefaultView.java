package org.cps.framework.boot;

import javax.faces.webapp.FacesServlet;

import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

import com.google.common.collect.ImmutableMap;
import com.sun.faces.config.ConfigureListener;
 
@Configuration
public class DefaultView   {
//	@Bean
//    public FacesServlet facesServlet() {
//        return new FacesServlet();
//    }
//
//    @Bean
//    public ServletRegistrationBean facesServletRegistration() {
//        ServletRegistrationBean registration = new ServletRegistrationBean(facesServlet(), "*.xhtml");
//        registration.setName("FacesServlet");
//        return registration;
//    }
//
//    @Bean
//    public ServletListenerRegistrationBean<ConfigureListener> jsfConfigureListener()         {
//        return new ServletListenerRegistrationBean<ConfigureListener>(new ConfigureListener());
//    } 

	
	@Bean
	public static CustomScopeConfigurer viewScope() {
	    CustomScopeConfigurer configurer = new CustomScopeConfigurer();
	    configurer.setScopes(
	            new ImmutableMap.Builder<String, Object>().put("view", new ViewScope()).build());
	    return configurer;
	}

}