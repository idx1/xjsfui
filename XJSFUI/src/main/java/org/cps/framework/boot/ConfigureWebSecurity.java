package org.cps.framework.boot;

import org.cps.framework.security.CustomAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity 
class ConfigureWebSecurity extends WebSecurityConfigurerAdapter {

	
	 @Override
     public void configure(WebSecurity web) throws Exception {
       web
         .ignoring()
            .antMatchers("/resources/**"); // #3
     }
	
	@Override
	  protected void configure(HttpSecurity http) throws Exception {
	    // require all requests to be authenticated except for the resources
	    http.authorizeRequests().antMatchers("/javax.faces.resource/**")
        .permitAll().anyRequest().authenticated();
    
	    http.authorizeRequests().antMatchers("/**/resource/**")
        .permitAll().anyRequest().authenticated();
//    
//	    http.authorizeRequests().antMatchers("/admin/resource/**")
//        .permitAll().anyRequest().authenticated();
//    
	    http.authorizeRequests().antMatchers("/login.xhtml")
        .permitAll().anyRequest().authenticated();
//    
		
		
		 
		
		
	    http
	      .authorizeRequests()
	      .anyRequest()
	      .authenticated()
	      .and()
	      .formLogin().loginPage("/login.xhtml").permitAll()
	        .failureUrl("/login.xhtml?error=true")
	      .successHandler(new RefererAuthenticationSuccessHandler());
	    
//	    // login
	    http.formLogin().loginPage("/login.xhtml").permitAll()
        .failureUrl("/login.xhtml?error=true");

	    http.formLogin().loginPage("/login.xhtml").permitAll()
        .failureUrl("/login.xhtml?error=true")
        .defaultSuccessUrl("/pages/admin.xhtml");

 	    http.authorizeRequests().antMatchers("/api/**").permitAll().anyRequest().authenticated();

	    
	    // logout
	    http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login.xhtml");
	    // not needed as JSF 2.2 is implicitly protected against CSRF
	    http.csrf().disable();
	  }
	
    @Autowired
    private CustomAuthenticationProvider customAuthenticationProvider;

    @Autowired
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(this.customAuthenticationProvider);
    }

}
