package org.cps.framework.boot;

import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;

public class ViewScope implements Scope{

	public Object get(String name, ObjectFactory objectFactory) {
		Map<String,Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getViewMap();
		
		System.out.println("viewMap : "+ viewMap);

		if(viewMap.containsKey(name)) {
		return viewMap.get(name);
		} else {
		Object object = objectFactory.getObject();
		viewMap.put(name, object);

		return object;
		}
		}

		public Object remove(String name) {
			
			System.out.println("remove : "+ name);

		return FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove(name);
		}

		public String getConversationId() {
			System.out.println("getConversationId : ");

		return null;
		}

		public void registerDestructionCallback(String name, Runnable callback) {
			System.out.println("registerDestructionCallback : ");

		//Not supported
		}

		@Override
		public Object resolveContextualObject(String key) {
			System.out.println("resolveContextualObject : ");
			return null;
		}}
