package org.cps.framework.boot;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.cps.framework.jms.message.UserMessage;
import org.cps.framework.jms.utils.DefaultErrorHandler;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.jms.support.destination.BeanFactoryDestinationResolver;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

@Configuration
public class JMSConfiguration {
	
	 @Value("${spring.activemq.broker.url}")
	  private String brokerUrl;
		
		@Value("${spring.activemq.user}")
		String userName;
		
		@Value("${spring.activemq.password}")
		String password;



//    @Autowired
//    private BeanFactory springContextBeanFactory;
    
    @Autowired
    GenericApplicationContext genericApplicationContext;

    @PostConstruct
    public void init() {
    	
    	System.out.println("############### JMS PC ke init me ###############");
        //genericApplicationContext.registerAlias("jmsConnectionFactory", "CPSFactory");
    }
    
    @Bean // Serialize message content to json using TextMessage
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        //converter.setTypeIdPropertyName("org.cps.framework.jms.message.UserMessage");
        
        
        
        Map<String, Class<?>> typeIdMappings = new HashMap<String,Class<?>>();
        typeIdMappings.put(UserMessage.class.getName(), UserMessage.class);
        converter.setTypeIdMappings(typeIdMappings);
        
        return converter;
    }
    
     
    @Bean
    public DefaultJmsListenerContainerFactory CPSFactory(ConnectionFactory connectionFactory , DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory =
                new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(activeMQConnectionFactory());
//        factory.setDestinationResolver(new BeanFactoryDestinationResolver(springContextBeanFactory));
        factory.setConcurrency("3-10");
        
        factory.setErrorHandler(new DefaultErrorHandler());
        
        configurer.configure(factory, connectionFactory);
        
        return factory;
    }
    
    @Bean
    public ActiveMQConnectionFactory activeMQConnectionFactory() {
      ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
      activeMQConnectionFactory.setBrokerURL(brokerUrl);
      activeMQConnectionFactory.setUserName(userName);
      activeMQConnectionFactory.setPassword(password);

      return activeMQConnectionFactory;
    }
    
    @Bean
    public ConnectionFactory connectionFactory(){
        return activeMQConnectionFactory();
    }

    
//    @Bean
//    public CachingConnectionFactory cachingConnectionFactory() {
//      return new CachingConnectionFactory(activeMQConnectionFactory());
//    }

    @Bean
    public JmsTemplate jmsTemplate() throws JMSException {
    	
    	
    	System.out.println("JMS Template IDENTITY");
    	
    	JmsTemplate jmsTemplate = new JmsTemplate(activeMQConnectionFactory());
    	
    	jmsTemplate.setMessageConverter(jacksonJmsMessageConverter());
    	
        return jmsTemplate;
    }
}
